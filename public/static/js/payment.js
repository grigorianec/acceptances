addEventListener('DOMContentLoaded', function () {
    let discountInput = document.getElementById('discount');

    // if users clicks on radio buttons, we should refresh a price
    changePriceOnRadioChange();

    // if users finishes to write in discount input, we should refresh the price
    discountInput.addEventListener('focusout', function() {
        changePrice();
    });
});

function changePrice() {
    let amountItem = document.querySelector('.bill__amount'),
        radioItems = document.querySelectorAll('input[name="months"]'),
        discountInput = document.getElementById('discount'),
        discountName = discountInput.value,
        amount,
        discount;

    // we're looking for checked subscription plan
    for (let i = 0; i < radioItems.length; i++) {
        if (radioItems[i].checked)
            amount = parseFloat(radioItems[i].dataset.bill);
    }

    // if user has tried to enter a discount
    if (discountName) {
        changeComment(discountInput, '...');

        // we use ajax to validate them
        $.get(
            "/static/discounts.json",
            {name: discountName},
            function (data) {
                // if we have the discount, modify the price
                if (data.discount) {
                    amountItem.textContent = (amount * (1 - data.discount)).toFixed(2);
                    changeComment(discountInput, 'Congratulations! You have ' + data.discount * 100 + '% discount.')
                }
                // if not, change the price without modifiers
                else {
                    amountItem.textContent = amount;
                    changeComment(discountInput, 'Sorry, we can\'t find the discount code you\'ve sent to us.')
                }
            }
        );
    } else {
        // if user hasn't tried to enter the discount, change the price without modifiers
        amountItem.textContent = amount;
        changeComment(discountInput, 'If you have the discount, type it here.')
    }
}

function changePriceOnRadioChange() {
    let inputs = document.querySelectorAll('input[name="months"]');

    for (let i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener('click', function() {
            changePrice();
        });
    }
}