addEventListener('DOMContentLoaded', function () {
    let primaryGradient, secondaryGradient, tertiaryGradient;

    let organizationsChartCanvas = document.getElementById('organizationsChart');

    if (organizationsChartCanvas) {
        organizationsChartCanvas = organizationsChartCanvas.getContext('2d');
        // gradients to fill the charts
        primaryGradient = createPrimaryGradient(organizationsChartCanvas);
        secondaryGradient = createSecondaryGradient(organizationsChartCanvas);
        tertiaryGradient = createTertiaryGradient(organizationsChartCanvas);

        let organizationsChart = new Chart(organizationsChartCanvas, {
            type: 'doughnut',
            data: {
                labels: ['FIAP', 'PSA', 'KSA'],
                datasets: [{
                    data: [32, 12, 9],
                    backgroundColor: [
                        primaryGradient,
                        secondaryGradient,
                        tertiaryGradient
                    ]
                }]
            },
            options: {
                legend: {
                    display: false
                },
                axes: {
                    display: false
                }
            }
        });
    }

    // let moneyChartCanvas = document.getElementById('moneyChart');
    //
    // if (moneyChartCanvas) {
    //     moneyChartCanvas = moneyChartCanvas.getContext('2d');
    //
    //     // gradients to fill the charts
    //     primaryGradient = createPrimaryGradient(moneyChartCanvas);
    //     secondaryGradient = createSecondaryGradient(moneyChartCanvas);
    //     tertiaryGradient = createTertiaryGradient(moneyChartCanvas);
    //
    //
    //     let moneyChart = new Chart(moneyChartCanvas, {
    //         type: 'bar',
    //         data: {
    //             labels: ['Successful salons', 'Unsuccessful salons'],
    //             datasets: [{
    //                 data: [125, 110],
    //                 backgroundColor: [
    //                     primaryGradient,
    //                     secondaryGradient,
    //                     tertiaryGradient
    //                 ]
    //             }]
    //         },
    //         options: {
    //             scales: {
    //                 yAxes: [{
    //                     ticks: {
    //                         beginAtZero: true
    //                     }
    //                 }],
    //                 xAxes: [{
    //                     ticks: {
    //                         display: false
    //                     }
    //                 }]
    //             },
    //             axes: {
    //                 display: false
    //             },
    //             legend: {
    //                 display: false
    //             }
    //         }
    //     });
    // }


});

function createPrimaryGradient(canvas) {
    return createGradient(canvas, '#495DD6', '#9849D6');
}

function createSecondaryGradient(canvas) {
    return createGradient(canvas, '#62F256', '#61E4DC');
}

function createTertiaryGradient(canvas) {
    return createGradient(canvas, '#F25656', '#CA61E4');
}

function createGradient(canvas, firstColor, secondColor) {
    let output = canvas.createLinearGradient(0.000, 0.000, 400.000, 400.000);
    output.addColorStop(0.000, firstColor);
    output.addColorStop(1.000, secondColor);

    return output;
}
