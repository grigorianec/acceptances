addEventListener('DOMContentLoaded', function () {
    // Fix animation on page load
    document.querySelector('body').classList.remove('preload');

    gdprConsent();

    // Change selects
    $(document).ready(function() {
        $('.js-select').select2({
                width: '100%',
                allowClear: true,
                placeholder: 'Choose...'
            }
        );
        $('.js-tags').select2({
                tags: true,
                width: '100%',
                allowClear: true,
                placeholder: 'Choose...'
            }
        );
    });

    // open menu on click
    let hamburger = document.querySelector('.js-hamburger');

    if (hamburger) {
        hamburger.addEventListener('click', function() {
            let sideMenu = document.querySelector('.main__sidemenu');

            if (sideMenu) {
               this.classList.toggle('hamburger_close');
               sideMenu.classList.toggle('main__sidemenu_shown');
            }
        });
    }

    // change date inputs
    $(".js-date").flatpickr({
        altInput: true,
        dateFormat: "Y-m-d",
        altFormat: "F j, Y"
    });

    monitorTheDates();

    togglePasswordButtons();

    sendNotifications();

    dismissNotifications();

    animateUploadFilesItem();

    clearSelect2OnReset();

    addIdToModals();
});

function clearSelect2OnReset() {
    $("select").closest("form").on("reset",function(ev){
        let targetJQForm = $(ev.target);
        setTimeout((function(){
            this.find("select").trigger("change");
        }).bind(targetJQForm),0);
    });
}

function changeComment(input, text) {
    let comment = input.parentElement.parentElement.querySelector('.form__comment span');

    if (!comment)
        return;

    comment.innerHTML = text;
}

function sendNotifications() {
    let sendNotificationLinks = document.querySelectorAll('a[href="#send-notification-modal"]');

    if (!sendNotificationLinks.length)
        return;

    for (let i = 0; i < sendNotificationLinks.length; i++) {
        sendNotificationLinks[i].addEventListener('click', function() {
            let sendNotificationModal = document.getElementById('send-notification-modal'),
                dateInput = sendNotificationModal.querySelector('#send-notification-date-type'),
                date = sendNotificationModal.querySelector('.js-date-span');

            dateInput.value = this.dataset.dateType;
            date.innerHTML = this.dataset.date;
        });
    }
}

function dismissNotifications() {
    let dismissNotificationLinks = document.querySelectorAll('a[href="#dismiss-notification-modal"]');

    if (!dismissNotificationLinks.length)
        return;

    for (let i = 0; i < dismissNotificationLinks.length; i++) {
        dismissNotificationLinks[i].addEventListener('click', function() {
            let dismissNotificationModal = document.getElementById('dismiss-notification-modal'),
                dateInput = dismissNotificationModal.querySelector('#dismiss-notification-date-type'),
                date = dismissNotificationModal.querySelector('.js-date-span');

            dateInput.value = this.dataset.dateType;
            date.innerHTML = this.dataset.date;
        });
    }
}

function animateUploadFilesItem() {
    let uploadFilesItems = document.querySelectorAll('.upload-files-item');

    if (!uploadFilesItems.length)
        return;

    for (let i = 0; i < uploadFilesItems.length; i++) {
        let fileInput = uploadFilesItems[i].querySelector('.upload-files-item__input');

        fileInput.addEventListener('change', function (e) {
            let fileName = e.target.value.split( '\\' ).pop();

            if (fileName) {
                let labelItem = uploadFilesItems[i].querySelector('.upload-files-item__name span'),
                    labelName = labelItem.innerHTML;

                labelName = labelName.split('(')[0].trim();

                labelItem.innerHTML = labelName + ' (' + fileName + ')';
            }
        })
    }
}

function monitorTheDates() {
    let closingDate = document.getElementById('closing_date'),
        resultsDate = document.getElementById('results_date'),
        awardDate = document.getElementById('award_date');

    // if there's only closing date, it's a past salon creation
    if (closingDate && !resultsDate && !awardDate) {
        let closingDatePickr = closingDate._flatpickr;

        // we won't allow user to choose a date after present day
        closingDatePickr.config.onChange.push(
            function(selectedDates, dateStr, instance) {
                let yesterday = new Date();
                yesterday.setDate(yesterday.getDate() - 1);

                if (new Date(selectedDates) >= new Date()) {
                    instance.setDate(yesterday)
                }
            }
        );

        return;
    }

    // if there're no dates inputs whatsoever
    // return nothing
    if (!closingDate && !resultsDate && !awardDate)
        return;

    let closingDatePickr = closingDate._flatpickr,
        resultsDatePickr = resultsDate._flatpickr,
        awardDatePickr = awardDate._flatpickr,
        allDatePickrInputs = [closingDatePickr, resultsDatePickr, awardDatePickr];


    // we won't allow user to choose a date before present day
    for (let datePickrInput of allDatePickrInputs) {
        datePickrInput.config.onChange.push(
            function(selectedDates, dateStr, instance) {
                if (new Date(selectedDates) < new Date()) {
                    instance.setDate(new Date())
                }
            }
        )
    }

    closingDatePickr.config.onChange.push(
        function(selectedDates, dateStr, instance) {
            // if results date is set
            if (resultsDatePickr.selectedDates.length) {
                // if closing date is bigger than results date
                // set closing date to results date
                if (new Date(selectedDates) > new Date(resultsDatePickr.selectedDates)) {
                    closingDatePickr.setDate(new Date(resultsDatePickr.selectedDates));
                }
                return;
            }

            // if award date is set but results date isn't
            if (awardDatePickr.selectedDates.length) {
                // if closing date is bigger than award date
                // set closing date to award date
                if (new Date(selectedDates) > new Date(awardDatePickr.selectedDates)) {
                    closingDatePickr.setDate(new Date(awardDatePickr.selectedDates));
                }
            }
        }
    );

    resultsDatePickr.config.onChange.push(
        function(selectedDates, dateStr, instance) {
            // if closing date is set
            if (closingDatePickr.selectedDates.length) {
                // if closing date is bigger than results date
                // set results date to closing date
                if (new Date(selectedDates) < new Date(closingDatePickr.selectedDates)) {
                    resultsDatePickr.setDate(new Date(closingDatePickr.selectedDates));
                }
            }

            // if award date is set
            if (awardDatePickr.selectedDates.length) {
                // if results date is bigger than award date
                // set results date to award date
                if (new Date(selectedDates) > new Date(awardDatePickr.selectedDates)) {
                    resultsDatePickr.setDate(new Date(awardDatePickr.selectedDates));
                }
            }
        }
    );

    awardDatePickr.config.onChange.push(
        function(selectedDates, dateStr, instance) {
            // if results date is set
            if (resultsDatePickr.selectedDates.length) {
                // if results date is less than award date
                // set award date to results date
                if (new Date(selectedDates) < new Date(resultsDatePickr.selectedDates)) {
                    awardDatePickr.setDate(new Date(resultsDatePickr.selectedDates));
                }

                return;
            }

            // if closing date is set but results date isn't
            if (closingDatePickr.selectedDates.length) {
                // if closing date is less than award date
                // set award date to closing date
                if (new Date(selectedDates) < new Date(closingDatePickr.selectedDates)) {
                    awardDatePickr.setDate(new Date(closingDatePickr.selectedDates));
                }
            }
        }
    );
}

function addIdToModals() {
    // find any link who has id and is referring to a modal
    const links = document.querySelectorAll('a[rel="modal:open"][data-id]');

    if (!links.length)
        return;

    // for every link we find
    for (let link of links) {
        let modalId = link.href.split('#')[1],
            modal = document.getElementById(modalId),
            form = modal.querySelector('form'),
            hiddenInputId = modalId.replace(/modal/g, 'id');

        // we create a hidden input if there's no input already
        if (!document.getElementById(hiddenInputId)) {
            let hiddenInput = document.createElement('input');

            hiddenInput.type = 'hidden';
            hiddenInput.id = hiddenInputId;
            hiddenInput.name = hiddenInputId;

            form.prepend(hiddenInput);
        }

        // on click we change id
        link.addEventListener('click', function() {
            let hiddenInput = document.getElementById(hiddenInputId);
            hiddenInput.value = this.dataset.id;
        })
    }
}

/**
 * Format date to YYYY-MM-DD
 *
 * @param date
 * @returns {string}
 */

function dateToYMD(date) {
    date = new Date(date);

    let d = date.getDate();
    let m = date.getMonth() + 1; // Month from 0 to 11
    let y = date.getFullYear();
    return '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
}

function findStartDate(dates) {
    return new Date(Math.min.apply(null,dates));
}

function findEndDate(dates) {
    return new Date(Math.max.apply(null,dates));
}

/**
 * Get a consent to use cookies
 */

function gdprConsent() {
    // if we have cookies 'salons-gdpr', user has made his choice
    // so we don't ask him again
    if (getCookie('salons-gdpr'))
        return;

    let gdpr = document.querySelector('.gdpr'),
        allowButton = gdpr.querySelector('.js-gdpr-allow'),
        denyButton = gdpr.querySelector('.js-gdpr-deny');

    // if he hasn't made his choice, we show the modal window
    gdpr.classList.remove('gdpr_hidden');

    // if he clicks 'agree', we save that information to cookies
    // we will ask him again in 365 days
    allowButton.addEventListener('click', function () {
        setCookie('salons-gdpr', 'allow', 365);
        gdpr.classList.add('gdpr_hidden');
    });

    // if he clicks 'deny'
    // we will ask him again in 30 days
    denyButton.addEventListener('click', function () {
        setCookie('salons-gdpr', 'deny', 30);
        gdpr.classList.add('gdpr_hidden');
    });
}


/**
 * Hide and show password on click
 */

function togglePasswordButtons() {
    // select all buttons which toggle password
    let toggleButtons = document.querySelectorAll('.form__toggle-password');

    // if there're no buttons, return
    if (!toggleButtons)
        return;

    for (let i = 0; i < toggleButtons.length; i++) {
        toggleButtons[i].addEventListener('click', function () {
            let icon = this.querySelector('i');

            if (icon.classList.contains('fa-eye')) {
                icon.classList.remove('fa-eye');
                icon.classList.add('fa-eye-slash');
                this.parentElement.querySelector('input[type="password"]').type = 'text';
            } else {
                icon.classList.remove('fa-eye-slash');
                icon.classList.add('fa-eye');
                this.parentElement.querySelector('input[type="text"]').type = 'password';
            }
        })
    }
}

/**
 * Tools to work with select2
 */

function putValueToSelect2(inputName, value) {
    const $input = $('#' + inputName);
    $input.val(value);
    $input.trigger('change');
}

/**
 * Tools to work with cookies
 */

function setCookie(name,value,days) {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for(let i=0;i < ca.length;i++) {
        let c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function showErrorInForm(form, content) {
    let errorItem = form.querySelector('.form__error');

    if (!errorItem) {
        console.log('The function can\'t find a place to show error');
        return;
    }

    errorItem.classList.remove('form__error_hidden');

    errorItem.innerHTML = content;
}

/**
 * Check if array includes all the items from the array.
 *
 * @param array
 * @param items
 * @returns boolean
 */
function isArrayIncludesItems(array, items) {
    return items.every(item => array.includes(item));
}