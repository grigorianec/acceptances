// FILE ANALYSIS FORM ELEMENTS
const form = document.getElementById('file-analysis-form');
const selects = document.querySelectorAll('select[name="user-columns"]');

// array of obligatory columns
const obligatoryColumns = ['salon_name', 'patronages', 'image_title'];

// value we get before the change event
let oldSelectValue;

addEventListener('DOMContentLoaded', function () {
    // we monitor selects
    // if something changes, we have to check if the columns don't repeat
    // (for instance, we can't have two 'Salon name' columns)
    for (select of selects) {
        // we store old select value in a variable
        $(select).on('select2:selecting', function() {
            oldSelectValue = $(this).val();
        });

        // if select changes, we check for repeats
        $(select).on('change', function() {
            checkRepeats(this);
        });
    }

    // when form submits, we should check if every obligatory column is sent to us
    // form.addEventListener('submit', function(e) {
    //     // if not, don't submit the form
    //     if (!checkObligatoryColumns()) {
    //         e.preventDefault();
    //         showErrorInForm(this, "It's required to assign values to 'Salon name', 'Image title' and 'Patronages' columns.");
    //     }
    // });
});

/**
 * We check if the item has or hasn't repeated values in other selects.
 * If it has, we assign old select value to the other select.
 *
 * @param item
 */
function checkRepeats(item) {
    // we can have as much patronages, skip and custom selects as we wish
    if (item.value === 'skip' || item.value === 'custom' || item.value === 'patronages')
        return;

    // but if it's one of the already created columns
    // we check everything
    for (select of selects) {
        // we don't check the actual select
        if (item === select)
            continue;

        if (item.value === select.value) {
            putValueToSelect2(select.id, oldSelectValue);
        }
    }
}

/**
 * Check if user sends us all obligatory columns.
 *
 * @returns boolean
 */
function checkObligatoryColumns() {
    let selectValues = [];

    for (select of selects) {
        selectValues.push(select.value);
    }

    return isArrayIncludesItems(selectValues, obligatoryColumns);
}