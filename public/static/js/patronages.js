addEventListener('DOMContentLoaded', function () {
    let patronages = document.querySelector('.js-patronages'),
        patronagesItems = patronages.querySelector('.patronages__items'),
        patronagesItem = patronages.querySelector('.patronages__item').cloneNode(true),
        addPatronage = document.querySelector('.js-add-patronage');

    // If edit page
    if (document.querySelector('input[name="_method"]'))
        toggleDeleteButtons();

    addPatronage.addEventListener('click', function() {
        let newItem = patronagesItem.cloneNode(true);
        newItem.classList.add('patronages__item_hidden');

        // Strip input of data
        newItem.querySelectorAll('input').forEach(i => {
            i.setAttribute('value', '');
            i.setAttribute('name', i.getAttribute('name').replace(/^([^\[\]]+)\[.+]$/, '_$1[]'));
        });

        patronagesItems.appendChild(newItem);
        newItem.classList.remove('patronages__item_hidden');

        toggleDeleteButtons();
    });

});

function toggleDeleteButtons() {
    let patronagesItems = document.querySelectorAll('.patronages__item'),
        patronagesNumber = patronagesItems.length;

    if (patronagesNumber > 1) {
        for (let i = 0; i < patronagesNumber; i++) {
            let deleteButton = patronagesItems[i].querySelector('.patronages__delete');

            deleteButton.classList.remove('patronages__delete_hidden');
        }
    } else {
        patronagesItems[0].querySelector('.patronages__delete').classList.add('patronages__delete_hidden');
    }

    let deleteButtons = document.querySelectorAll('.patronages__delete');

    for (let i = 0; i < deleteButtons.length; i++) {
        // Closure to guarantee correct JS behaviour
        deleteButtons[i].addEventListener('click', deletePatronage)
    }
}

function deletePatronage(event) {
    let patronageItem = event.target.parentElement;

    patronageItem.remove(true);

    // We must log which patronages are to be deleted
    let input = document.querySelector('input[name="deletable"]');
    if (!input.value)
        input.value = '[]';

    let id = parseInt(patronageItem.querySelector('#organization').name.replace(/^[^\[\]]+\[(.+)]$/, '$1'));

    let data = JSON.parse(input.value);
    data.push(id);
    input.value = JSON.stringify(data);

    toggleDeleteButtons();
}

function removePatronageItemsExceptFirst() {
    const patronageItems = document.querySelectorAll('.patronages__item');

    patronageItems.forEach(function (item, i, array) {
        // if it's the first item, we skip it
        if (i === 0)
            return;

        // everything else we remove
        item.remove(true);
    });

    toggleDeleteButtons();
}