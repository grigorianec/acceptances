addEventListener('DOMContentLoaded', function () {
    // Send an email on forgot page form submit
    var forgotPasswordForm = document.querySelector('#forgot-password-form');

    if (forgotPasswordForm) {
        forgotPasswordForm.addEventListener('submit', function (e) {
            e.preventDefault();

            // receive the email from the form
            var formEmail = this.querySelector('#forgot-password-email').value;

            // todo: here should be code to send an email

            // if everything is okay or not
            var isSuccess = true;

            // set his or her email in output messages
            var emails = this.parentElement.querySelectorAll('.modal__email-highlighted');

            for (var i = 0; i < emails.length; i++) {
                emails[i].innerHTML = formEmail;
            }

            // take output
            var modalOutput = this.parentElement.querySelector('.modal__output');

            // waiting message
            modalOutput.className = '';
            modalOutput.classList.add('modal__output');
            modalOutput.classList.add('modal__output_waiting');

            // time for checking
            // TODO: delete
            setTimeout(function () {
                if (isSuccess) {
                    modalOutput.className = '';
                    modalOutput.classList.add('modal__output');
                    modalOutput.classList.add('modal__output_success');
                } else {
                    modalOutput.className = '';
                    modalOutput.classList.add('modal__output');
                    modalOutput.classList.add('modal__output_error');
                }
            }, 500);
        });

        var formResend = document.querySelector('.modal__resend');

        if (formResend) {
            formResend.addEventListener('click', function() {
                forgotPasswordForm.querySelector('input[type="email"]').value = forgotPasswordForm.parentElement.querySelector('.modal__email-highlighted').innerHTML;
                forgotPasswordForm.querySelector('input[type="submit"]').click();
            })
        }
    }
});
