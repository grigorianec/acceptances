<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Console\Commands\AccountSuspended;
use App\Mail\EmailAccountSuspended;
use App\Mail\EmailSalonAward;
use App\Mail\EmailSalonResultDate;
use App\Mail\EmailUpcomingSalon;
use App\Mail\EmailWithCloseSalon;
use App\Mail\EmailWithExpiringPaymend;

Route::get('/', function () {

});

// TODO debug
Route::get('/template/{name}', function($name) {
    $name = str_replace('.html', '', $name);
    return view("$name");
});


// TODO debug
Route::get('/mail/{id}', function ($id) {

    $salonName = 'Global Crooks-Jenkins Awards';
    $dateTest  = now();
    switch ($id) {

        case 'accountSuspended':
            return new EmailAccountSuspended();
            break;
        case 'closeSalon':
            return new EmailWithCloseSalon($salonName, $dateTest);
            break;
        case 'paymentReminder':
            return new EmailWithExpiringPaymend($dateTest->addDays(7));
            break;
        case 'salonAward':
            return new EmailSalonAward($salonName, $dateTest);
            break;
        case 'salonResult':
            return new EmailSalonResultDate($salonName, $dateTest);
            break;
        case 'upcomingSalon':
            return new EmailUpcomingSalon($salonName, $dateTest);
            break;
    }
});


Route::get('/', function() {return file_get_contents(public_path('home/index.html'));});


Route::prefix('app') -> group(function(){

    // Auth and socialite
    Auth::routes(['verify' => TRUE]);
    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('/redirect/{service}', 'Auth\SocialiteLoginController@redirect')
        ->name('socialite.redirect');
    Route::get('/callback/{service}', 'Auth\SocialiteLoginController@callback')
        ->name('socialite.callback');

    Route::get('terms-of-service', function() {
        return view('terms-of-service');
    });

    Route::get('forgot-password', function() {
        return view('forgot-password');
    })->name('forgot-password');


    Route::middleware(['verified','user']) -> group(function() {

        // Payment
        Route::get('payment/{months}', 'PaymentController') -> name('payment')
            -> where('months', '[0-9]+');

        Route::get('renewal', 'PaymentController@renewal') -> name('renewal');

        Route::post('/pay', 'PaymentController@pay') -> name('pay');

        Route::get('/execute-payment/{success}/', 'PaymentController@execute')->name('execute.payment');

        Route::get('/payment-cancel','PaymentController@cancel')->name('payment.cancel');


        // Support
        Route::post('support', 'SupportController') -> name('support');

        // Profile export
        Route::get('settings/export', 'SettingsController@export')->name('settings.export');


        // Paid content
        Route::middleware('paid') -> group(function(){

            // Main routes
            Route::get('/', 'HomeController')->name('profile');
            Route::get('/profile', 'HomeController');

            Route::get('/personal-details', 'SettingsController@profileData') -> name('personal-details');
            Route::post('/personal-details', 'SettingsController@submitProfileData') -> name('personal-details.submit');

            foreach(['settings', 'statistics', 'gallery', 'future-salons', 'past-salons', 'acceptances'] as $x)
                Route::get(
                    "$x",
                    implode('', array_map('ucfirst', explode('-', $x))).'Controller'
                )->name($x);

            // Settings
            Route::prefix('settings') -> group(function() {
                Route::get('/', 'SettingsController')->name('settings');
                Route::get('account', 'SettingsController@account')->name('settings.account');
                Route::get('payment', 'SettingsController@payment')->name('settings.payment');

                Route::get('data', 'SettingsController@data')->name('settings.data');
                Route::post('/', 'SettingsController@submit')->name('settings.submit');
                Route::delete('/', 'SettingsController@dropAccount')->name('settings.dropAccount');
            });

            // Statistics
            Route::prefix('statistics') -> group(function() {
                Route::get('/', 'StatisticsController') -> name('statistics');
                foreach (['money', 'salons', 'photos','countries'] as $x)
                    Route::get($x, "StatisticsController@$x");
            });

            Route::get('salon/{salonId}', 'FutureSalonsController@delete')->name('delete.future.salon'); // TODO

            // Future salons
            Route::prefix('future-salons') -> group(function() {
                Route::get('{page?}', 'FutureSalonsController') -> name('future-salons')
                    -> where('page', '[0-9]+');
                Route::get('calendar', 'FutureSalonsController@calendar') -> name('calendar');
                Route::get('add', 'FutureSalonsController@add') -> name('future-salons.add');
                Route::get('future-salon/{salonId}', 'FutureSalonsController@item')
                    -> where('salonId', '[0-9]+')->name('future-salons.item');

                Route::get('salon/{salonId}/edit', 'FutureSalonsController@edit')
                    -> where('salonId', '[0-9]+') -> name('future-salons.edit');
                Route::get('salon/{salonId}/edit/details', 'FutureSalonsController@editDetails')
                    -> where('salonId', '[0-9]+') -> name('future-salons.edit-details');

                Route::post('create', 'FutureSalonsController@create')
                    -> name('future-salons.create');

                Route::post('change-notification', 'FutureSalonsController@changeNotification')
                    ->name('change-notification');
                Route::patch('salon/{salonId}', 'FutureSalonsController@modify')
                    -> name('future-salons.modify');

            });

            // Past salons
            Route::prefix('past-salons') -> group(function() {
                Route::get('{page?}', 'PastSalonsController') -> name('past-salons')
                    -> where('page', '[0-9]+');
                Route::get('add', 'PastSalonsController@add') -> name('past-salons.add');
                Route::get('salon/{salonId}', 'PastSalonsController@item')
                    -> where('salonId', '[0-9]+') -> name('past-salons.item');
                Route::get('salon/{salonId}/edit', 'PastSalonsController@edit')
                    -> where('salonId', '[0-9]+') -> name('past-salons.edit');
                Route::get('salon/{salonId}/edit/details', 'PastSalonsController@editDetails')
                    -> where('salonId', '[0-9]+') -> name('past-salons.edit-details');
                Route::get('salon/{salonId}/edit/materials', 'PastSalonsController@editMaterials')
                    -> where('salonId', '[0-9]+') -> name('past-salons.edit-materials');

                Route::post('/upload-attachment/{id}', 'PastSalonsController@uploadAttachment')->name('upload.attachment');

                Route::post('create', 'PastSalonsController@create')
                    -> name('past-salons.create');
                Route::patch('salon/{salonId}', 'PastSalonsController@modify')
                    -> name('past-salons.modify');
                Route::get('delete/salon/{salonId}', 'PastSalonsController@delete')->name('delete.past.salon');
                Route::get('salon-attachments/{type}/{id}', 'PastSalonsController@downloadAttachment')->name('downloadAttachment');

                Route::delete('salon/{salonId}', function($id) {
                    return redirect("/app/salon/$id", 307);
                });
            });

            // Gallery
            Route::prefix('gallery') -> group(function(){
                Route::get('{page?}', 'GalleryController') -> name('gallery')
                    -> where('page', '[0-9]+');
                Route::get('item/{id}', 'GalleryController@item')
                    -> where('id', '[0-9]+')->name('gallery-item');
                Route::post('/change-image/{id}', 'GalleryController@updateImage')->name('updateImage')
                    -> where('id', '[0-9]+');
                Route::post('/change-image-title', 'GalleryController@updateImageTitle')->name('update.imageTitle')
                    -> where('id', '[0-9]+');
                Route::get('/download/{id}', 'GalleryController@download')->name('download.image')
                    -> where('id', '[0-9]+');
                Route::delete('/photo/', 'GalleryController@destroy')->name('delete.photo');
            });

           // Acceptances
            Route::prefix('acceptances') -> group(function(){
            Route::get('/', 'AcceptancesController') -> name('acceptances');
            Route::get('/upload-accept', 'AcceptancesController@acceptUpload') -> name('upload.acceptances');
            Route::post('/save-uploaded-accepts', 'AcceptancesController@saveUploadedAccepts') -> name('save.uploaded.acceptances');
            Route::post('/update-accept', 'AcceptancesController@updateAccept') -> name('update.acceptance');
            Route::delete('/{id}/destroy', 'AcceptancesController@destroyAccept') -> name('destroy.acceptance');
            Route::post('/upload', 'AcceptancesController@upload') -> name('upload');
            Route::post('/add-column', 'AcceptancesController@addColumn') -> name('addColumn');
            Route::post('/update-column', 'AcceptancesController@updateCustomColumn') -> name('update.column');
            Route::post('/add-accept', 'AcceptancesController@addAccept') -> name('add.accept');
            Route::delete('/delete', 'AcceptancesController@deleteCustomColumn') -> name('delete.column');
        });

        });

    });

    //Admin content
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin','middleware' => "admin"], function () {
        //Users
        Route::get('/' , 'UsersController@index')->name('admin.users');
        Route::get('/profile-user/{id}','UsersController@profile')->name('admin.user.profile');
        Route::post('/edit-payment-period', 'UsersController@editPayment')->name('edit.payment.period');
        Route::delete('profile-user/delete/{id}', 'UsersController@destroy')->name('profile.user.delete');
        Route::get('enter-user-profile/{id}', 'UsersController@enterUserProfile')->name('enter.profile');

        //discounts
        Route::get('discount',  'DiscountsController@index')->name('admin.discounts');
        Route::get('/create/discount','DiscountsController@create')->name('create.discount');
        Route::post('/create/discount','DiscountsController@store')->name('store.discount');
        Route::delete('/delete/','DiscountsController@destroy')->name('delete.discounts');
    });
});
