<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CountriesStatsView
 *
 * @property int $id
 * @property string $name
 * @property int $award_count
 * @property int $salons_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereAwardCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereName($value)
 * @mixin \Eloquent
 * @property int|null $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereUserId($value)
 */
class CountriesStatsView extends Model
{

    protected $table = 'countries_stats';
}
