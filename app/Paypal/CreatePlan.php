<?php


namespace App\Paypal;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Common\PayPalModel;
use function CoffeeScript\t;

class CreatePlan extends Paypal
{
    protected  $months;
    protected  $price;
    protected  $description;


    public function __construct($months, $description , $price)
    {

        $this->months = $months;
        $this->price  = $price;
        $this->description = $description;

        parent::__construct();

    }

    public function createPlan() {
        // Create a new billing plan
        $plan = $this->plan();

        // Set billing plan definitions
        $paymentDefinition = $this->paymentDefinition();


        /***/
//        $chargeModel = $this->chargeModel();
//        $paymentDefinition->setChargeModels(array($chargeModel));

        // Set merchant preferences
        $merchantPreferences = $this->merchantPreferences();


        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        $createdPlan = $plan->create($this->apiContext);

        $plan = $this->activate($createdPlan->getId());
        return $plan;

    }

    /**
     * @return Plan
     * @name
     * @desciptions
     * @setTupe
     */
    protected function plan() :Plan
    {
        $plan = new Plan();
        $plan->setName('Monthly Billing')
            ->setDescription('Subscription'. $this->description)
            ->setType('infinite');
        return $plan;
    }

    /**
     * @return PaymentDefinition
     * @setType
     * How frequently the customer should be charged.
     * Number of cycles in this payment definition
     *
     * Amount that will be charged at the end of each cycle for this payment definition
     */
    protected function paymentDefinition() :PaymentDefinition
    {
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Regular Payments')
            ->setType('REGULAR')
            ->setFrequency('Month')
            ->setFrequencyInterval($this->months)
            ->setCycles(0)
            ->setAmount(new Currency(array('value' => $this->price, 'currency' => 'USD')));
        return $paymentDefinition;
    }
    /**
     * @return ChargeModel
     * @setype
     */
    protected function chargeModel() :ChargeModel
    {
        $chargeModel = new ChargeModel();
        $chargeModel->setType('SHIPPING')
            ->setAmount(new Currency(array('value' => 3, 'currency' => 'USD')));
        return $chargeModel;
    }
    /**
     * @return MerchantPreferences
     * @url success
     * @url cancel
     *
     */
    protected function merchantPreferences() :MerchantPreferences
    {
        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setReturnUrl('https://mysalonresults.com/app/execute-payment/true')
            ->setCancelUrl('https://mysalonresults.com/app/cancel/false')
            ->setAutoBillAmount("yes")
            ->setInitialFailAmountAction("CONTINUE")
            ->setMaxFailAttempts("0");
                //    * Setup fee amount. Default is 0.
        //    ->setSetupFee(new Currency(['value' => $this->price, 'currency' => 'USD']));
        return $merchantPreferences;

    }
    /**
     * @return \PayPal\Api\PlanList
     *
     */
    public function listPlan() {
        $params = ['page_size' => '2'];
        $planList = Plan::all($params, $this->apiContext);
        return $planList;
    }
    /**
     * @param $id
     * @return Plan
     */
    protected function planDetail($id) {
        $plan = Plan::get($id, $this->apiContext);
        return $plan;
    }

    /**create the plan
     * @param $planId
     * @return Plan
     */
    public function activate($planId)
    {
        $createdPlan = $this->planDetail($planId);
        $patch = new Patch();
        $value = new PayPalModel('{
	       "state":"ACTIVE"
	     }');

        $patch->setOp('replace')
            ->setPath('/')
            ->setValue($value);
        $patchRequest = new PatchRequest();
        $patchRequest->addPatch($patch);

        $createdPlan->update($patchRequest, $this->apiContext);
        $plan = Plan::get($createdPlan->getId(), $this->apiContext);
        return $plan;
    }
}
