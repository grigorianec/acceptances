<?php


namespace App\Paypal;

use PayPal\Api\Agreement;
use PayPal\Api\AgreementStateDescriptor;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;

class PaypalAgreement extends Paypal
{

    public function paypalRedirect($id) {

    return redirect($this->agreement($id));
    }

    /**  Create new agreement
     * @param $id
     * @return string
     * @setName
     * @setDescription
     * @setStartDate
     * @setPlan
     * @setShippingAddress
     * @setPayer
     */
    protected function agreement($id) :string
    {
        $agreement = new Agreement();
        $agreement->setName('Base Agreement')
            ->setDescription('Subscription')
            ->setStartDate(\Carbon\Carbon::now()->addSeconds(10)->toIso8601String());

        $agreement->setPlan($this->plan($id));
        $agreement->setPayer($this->payer());
        $agreement->setShippingAddress($this->shippingAddress());

        // Create agreement
        $agreement = $agreement->create($this->apiContext);

        // Extract approval URL to redirect user
        $approvalUrl = $agreement->getApprovalLink();

        return $approvalUrl;

    }

    /**Set plan id
     * @param $id
     * @return Plan
     */
    protected function plan($id) :Plan
    {
        $plan = new Plan();
        $plan->setId($id);
        return $plan;
    }

    /** Add payer type
     * @return Payer
     */
    protected function payer() :Payer
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        return $payer;
    }

    /**
     * @return ShippingAddress
     */
    protected function shippingAddress() :ShippingAddress
    {
        $shippingAddress = new ShippingAddress();
        $shippingAddress->setLine1('First Street')
            ->setCity('Saratoga')
            ->setState('CA')
            ->setPostalCode('95070')
            ->setCountryCode('US');
        return $shippingAddress;
    }

    public function executeAgreement($token) {
        $agreement = new Agreement();
        $agreement->execute($token, $this->apiContext);
        return $agreement->getId();
    }
    public function suspend($billingID){
        $agreementStateDescriptor = new AgreementStateDescriptor();
        $agreementStateDescriptor->setNote("Suspending the agreement");
        $agreement = new Agreement();
        $agreement->setId($billingID);
        $agreement->suspend($agreementStateDescriptor, $this->apiContext);
        return $agreement = $this->getPlanDetails($billingID);
    }

    public function getPlanDetails($id){
        $agreement = new Agreement();
        return $agreement = Agreement::get($id, $this->apiContext);
    }



}
