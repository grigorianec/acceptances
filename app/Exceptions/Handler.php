<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof AuthenticationException || $e instanceof AuthorizationException)
            return parent::render($request, $e);
        try {
            $l = Auth::check();
        } catch(\Throwable $x) {
            $l = false;
        }
        if ($e instanceof HttpException && $e -> getStatusCode() == 404)
            return response() -> view('error', ['n' => 404, 'isLoggedIn' => $l]);
        if (env('APP_DEBUG'))
            return parent::render($request, $e);
        $ea = ['n' => 500, 'e' => $e->getMessage(), 'isLoggedIn' => $l];
        return response() -> view('error', $ea);
    }
}
