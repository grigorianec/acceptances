<?php

use App\Attachment;
use App\Notification;

function discount($coupon, $subscriberPlans) {
    $total    = $subscriberPlans['months'] * $subscriberPlans['price'];
    $discount = $coupon->discount_amount / 100 * $total;
    $discountTotalPerMonth = $total - $discount;
    $price   = $discountTotalPerMonth / $subscriberPlans['months'];
    return $price;
}

function checkNotification( $salonId, $type) {
   $notification = Notification::getNotificationSalon($salonId, $type);
   if($notification) {
       return true;
   }
   return false;
}

function checkAttachmentsSalon($salonId, $type) {
    $attachments = Attachment::getAttachmentsSalon($salonId, $type);
    if($attachments->isNotEmpty()) {
        return true;
    }
    return false;
}


