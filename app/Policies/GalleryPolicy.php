<?php

namespace App\Policies;

use App\Participation;
use App\Photo;
use App\PhotoStatsView;
use App\Salon;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GalleryPolicy
{
    use HandlesAuthorization;

    public function modifyViewPhoto(User $user, Photo $photo)
    {
        return $user->id == $photo->user_id;
    }
}
