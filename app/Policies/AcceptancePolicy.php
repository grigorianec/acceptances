<?php

namespace App\Policies;

use App\Acceptance;
use App\Participation;
use App\Salon;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AcceptancePolicy
{
    use HandlesAuthorization;

    public function updateAccept(User $user, Acceptance $acceptance)
    {
        $salon = Salon::where('id',
            Participation::where('id', $acceptance->participation_id)
                ->first()->salon_id)->first();
        return $user->id == $salon->user_id;
    }

    public function destroyAccept(User $user, Acceptance $acceptance)
    {
        $salon = Salon::where('id',
            Participation::where('id', $acceptance->participation_id)
                ->first()->salon_id)->first();
        return $user->id == $salon->user_id;
    }

    public function addAccept(User $user, Salon $salon)
    {
        return $user->id == $salon->user_id;
    }
}
