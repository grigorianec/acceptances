<?php

namespace App\Policies;

use App\Salon;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SalonsPolicy
{
    use HandlesAuthorization;

    public function modify(User $user, Salon $salon)
    {
        return $user->id == $salon->user_id;
    }
}
