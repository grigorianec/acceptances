<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CountriesStatsView
 *
 * @property int $id
 * @property string $name
 * @property int $award_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereAwardCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereName($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property float|null $acceptances_count
 * @property float|null $award_count_FIAP
 * @property float|null $award_count_PSA
 * @property string|null $patronage_PSA
 * @property string|null $patronage_FIAP
 * @property int|null $entry_fee
 * @property int|null $best_author
 * @property string|null $comment
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView whereAcceptancesCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView whereAwardCountFIAP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView whereAwardCountPSA($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView whereBestAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView whereEntryFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView wherePatronageFIAP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView wherePatronagePSA($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView whereUserId($value)
 * @property string|null $end_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView whereEndDate($value)
 * @property string|null $closing_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SalonsStatsView whereClosingDate($value)
 */
class SalonsStatsView extends Model
{
    protected $table = 'salons_stats';
    protected $primaryKey = 'id';

    use SalonItem;


    public function patronages() {
        return $this -> hasMany(Patronage::class, 'salon_id', 'id');
    }

    public function attachments() {
        return $this -> hasMany(Attachment::class);
    }


    public function acceptances() {
        return $this -> hasManyThrough(Acceptance::class, Participation::class,'salon_id');
    }


    public function participations() {
        return $this -> hasMany(Participation::class,'salon_id', 'id');
    }
}
