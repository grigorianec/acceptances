<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Salon
 *
 * @property int $id
 * @property string $name
 * @property string|null $patronage_FIAP
 * @property string|null $patronage_PSA
 * @property string|null $comment
 * @property int|null $entry_fee
 * @property string|null $fee_currency
 * @property string|null $best_author
 * @property Carbon|null $start_date
 * @property Carbon|null $closing_date
 * @property Carbon|null $results_date
 * @property Carbon|null $award_date
 * @property string|null $deadline
 * @property int $user_id
 * @property string $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereBestAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereEntryFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereFeeCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon wherePatronageFIAP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon wherePatronagePSA($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereUserId($value)
 * @mixin \Eloquent
 * @property int $country_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereCountryId($value)
 * @property string|null $tracking_number
 * @property-read mixed $award_date_formatted
 * @property-read mixed $closing_date_formatted
 * @property-read mixed $country_year
 * @property-read mixed $patronages
 * @property-read mixed $results_date_formatted
 * @property-read mixed $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereAwardDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereClosingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereResultsDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salon whereTrackingNumber($value)
 */
class Salon extends Model
{
    use SalonItem;

    const DATE_FORMAT = 'd.m.Y';

    protected $fillable = [
        'name', 'user_id', 'country_id', 'image', 'entry_fee', 'is_future', 'best_author', 'catalogue_link', 'award_were_delivered','title'
    ];


    protected $dates = ['start_date', 'closing_date', 'award_date', 'results_date'];

    public function notifications() {
        return $this->hasMany(Notification::class,'salon_id');
    }

    public function user() {
        return $this -> belongsTo(User::class);
    }

    public function country() {
        return $this -> belongsTo(Country::class);
    }

    public function acceptances() {
        return $this -> hasManyThrough(Acceptance::class, Participation::class);
    }

    public function participations() {
        return $this -> hasMany(Participation::class);
    }

    public function attachments() {
        return $this -> hasMany(Attachment::class);
    }

    public function patronages() {
        return $this -> hasMany(Patronage::class);
    }

    public static function getSalonWitchNotifications($type, $date) {
      return Salon::with('user')->whereHas('notifications', function( $query ) use ($type) {
          $query->where('type', $type);
      })->whereDate($date, Carbon::now()->addDays(1)->format('Y-m-d'))->get();
    }

    public function getTypeAttribute() {
        if ($this -> closing_date && (!$this -> award_date || $this -> closing_date <= $this -> award_date)
            && (!$this -> results_date || $this -> closing_date <= $this -> results_date))
            return 'closing';
        elseif ($this -> award_date && (!$this -> closing_date || $this -> award_date <= $this -> closing_date)
            && (!$this -> results_date || $this -> award_date <= $this -> results_date))
            return 'award';
        elseif ($this -> results_date)
            return 'results';
        else
            return 'none';
    }

    public function getUpcomingAttribute() {
        return ((!$this -> closing_date || $this -> closing_date >= now())
            && (!$this -> results_date || $this -> results_date >= now())
            && (!$this -> award_date || $this -> award_date >= now()));
    }

    public function getCountryYearAttribute() {
        if (($y = $this -> closing_date) && $c = $this->country()->first())
            return $c->name . ', ' . $y -> year;
        else if ($c = $this->country()->first())
            return $c->name;
        else if ($y = $this -> closing_date)
            return $this -> closing_date -> year;
        return '';
    }

    public function checkedBestAuthor($value){
        $this->best_author = empty($value) ? 0 : 1;
        $this->save();
    }

    public function checkedAwardWereDelivered($value) {
        $this->award_were_delivered = empty($value) ? 'no' : 'yes';
        $this->save();
    }

    public function getStartDateFormattedAttribute() {
        return $this->start_date ? $this->start_date->format(self::DATE_FORMAT): '';
    }

    public function getClosingDateFormattedAttribute() {
        return $this->closing_date ? $this->closing_date->format(self::DATE_FORMAT): '';
    }

    public function getAwardDateFormattedAttribute() {
        return $this->award_date ? $this->award_date->format(self::DATE_FORMAT): '';
    }

    public function getResultsDateFormattedAttribute() {
        return $this->results_date ? $this->results_date->format(self::DATE_FORMAT): '';
    }

    public function getClosingDateOrNullAttribute() {
        return $this->closing_date ? $this->closing_date : '';
    }

    public function getAwardDateOrNullAttribute() {
        return $this->award_date ? $this->award_date : '';
    }

    public function getResultsDateOrNullAttribute() {
        return $this->results_date ? $this->results_date : '';
    }

    public function getCommentOrNullAttribute() {
        return $this->comment ? $this->comment : '';
    }

    public function getTrackingNumberOrNullAttribute() {
        return $this->tracking_number ? $this->tracking_number : '';
    }

    public function getEntryFeeOrNullAttribute() {
        return $this->entry_fee ? $this->entry_fee : '';
    }

    public function getBestAuthorOrNullAttribute() {
        return $this->best_author ? $this->best_author : false;
    }

    public function getCatalogueLinkOrNullAttribute() {
        return $this->catalogue_link ? $this->catalogue_link : '';
    }
}
