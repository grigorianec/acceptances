<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Patronage
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Patronage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Patronage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Patronage query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $salon_id
 * @property string $organization
 * @property string $number
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Patronage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Patronage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Patronage whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Patronage whereOrganization($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Patronage whereSalonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Patronage whereUpdatedAt($value)
 */
class Patronage extends Model
{
    protected $fillable = ['organization', 'number', 'salon_id'];

    public function salon() {
        return $this -> belongsTo(Salon::class);
    }
}
