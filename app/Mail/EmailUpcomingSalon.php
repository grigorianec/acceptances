<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailUpcomingSalon extends Mailable
{
    use Queueable, SerializesModels;

    public $salon;
    public $start_date;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($salon, $start_date)
    {
        $this->salon = $salon;
        $this->start_date = $start_date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Notice')
            ->markdown('_mail.upcoming_salon ');
    }
}
