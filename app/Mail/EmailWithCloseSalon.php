<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailWithCloseSalon extends Mailable
{
    use Queueable, SerializesModels;

    public $salon;
    public $closeDate;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($salon, $closeDate)
    {
        $this->salon     = $salon;
        $this->closeDate = $closeDate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Notice')
            ->markdown('_mail.closing_salon');
    }

}
