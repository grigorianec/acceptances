<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailSalonAward extends Mailable
{
    use Queueable, SerializesModels;

    public $salon;
    public $award_date;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($salon, $award_date)
    {
        $this->salon = $salon;
        $this->award_date = $award_date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Notice')
            ->markdown('_mail.salon_award');
    }
}
