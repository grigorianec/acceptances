<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailWithExpiringPaymend extends Mailable
{
    use Queueable, SerializesModels;

    public $expired;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($expired)
    {
        $this->expired = $expired;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Reminder Payment expiring ')
            ->markdown('_mail.expiring_paymend');
    }
}
