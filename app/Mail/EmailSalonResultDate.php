<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailSalonResultDate extends Mailable
{
    use Queueable, SerializesModels;

    public $salon;
    public $result_date;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($salon, $result_date)
    {
        $this->salon = $salon;
        $this->result_date = $result_date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Notice')
            ->markdown('_mail.salon_result');
    }
}
