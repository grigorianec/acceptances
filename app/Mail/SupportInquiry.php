<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SupportInquiry extends Mailable
{
    use Queueable, SerializesModels;

    public $body;
    public $id;
    public $name;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($body)
    {
        $this->body  = $body;
        $this->id    = \Auth::id();
        $this->name  = \Auth::user()->name;
        $this->email = \Auth::user()->email;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email, config("app.name"))
            ->subject('Support')
            ->markdown('_mail.support');
    }
}
