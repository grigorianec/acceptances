<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ColumnsValues extends Model
{
    protected $fillable = ['value', 'acceptance_id', 'column_id'];

    public function column() {
        return $this -> belongsTo(Column::class);
    }
}
