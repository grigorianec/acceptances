<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public function boot()
    {
        view()->composer('*', function($view) {
            view() -> share('route', '/app/'.Route::currentRouteName());
        });
    }

    public function register()
    {

    }

}
