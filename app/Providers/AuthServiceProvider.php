<?php

namespace App\Providers;

use App\Acceptance;
use App\Participation;
use App\Photo;
use App\Policies\AcceptancePolicy;
use App\Policies\GalleryPolicy;
use App\Policies\SalonsPolicy;
use App\Salon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Acceptance::class => AcceptancePolicy::class,
        Salon::class => SalonsPolicy::class,
        Photo::class => GalleryPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
