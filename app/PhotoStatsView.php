<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CountriesStatsView
 *
 * @property int $id
 * @property string $name
 * @property int $award_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereAwardCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereName($value)
 * @mixin \Eloquent
 * @property int|null $user_id
 * @property int|null $photo_id
 * @property string|null $title
 * @property string|null $image
 * @property int $acceptances_count
 * @property int $awards_count
 * @property int $participation_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PhotoStatsView whereAcceptancesCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PhotoStatsView whereAwardsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PhotoStatsView whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PhotoStatsView whereParticipationCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PhotoStatsView wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PhotoStatsView whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PhotoStatsView whereUserId($value)
 */
class PhotoStatsView extends Model
{
    protected  $guarded = [];

    protected $table = 'photo_stats';
}
