<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    protected $guarded = [];

    public function  salon(){
        return $this->belongsTo(Salon::class);
    }

    /**
     * @param $salon_Id
     * @param $type  (closing_date, results_date, award_date)
     * @return mixed
     */
    public static function createNotification($salon, $type) {
       return Notification::updateOrCreate(['salon_id' => $salon, 'type' => $type]);
    }

    public static function getNotificationSalon($salon, $type){
       return Notification::where('salon_id',$salon)
            ->where('type', $type)->first();
    }

    public static function turnOffNotification($salon, $type) {
       $not = self::getNotificationSalon($salon, $type);
       if(!is_null($not)){
           $not->delete();
       }
        return false;
    }

}
