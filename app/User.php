<?php

namespace App;

//use App\Notifications\CustomerResetPasswordNotification;
use App\Paypal\PaypalAgreement;
use Carbon\Carbon;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string|null $surname
 * @property string|null $email
 * @property string|null $password
 * @property string|null $facebook_token
 * @property string|null $google_token
 * @property int|null $country_id
 * @property string $paid_until
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFacebookToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGoogleToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePaidUntil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'google_token', 'facebook_token','paid_until'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [
        'paid_until'
    ];


    public function vouchers() {
        return $this->belongsToMany(Voucher::class);
    }

    public function country(): Relation {
        return $this -> belongsTo(Country::class);
    }

    public function salons(): Relation {
        return $this -> hasMany(Salon::class);
    }
    public function subscriptions() {
        return $this->hasOne(Subscription::class)->where('cancel',false);
    }

    public function participations(): Relation {
        return $this -> hasManyThrough(Participation::class, Salon::class);
    }


    public function futureSalons(): Relation {
        return $this -> hasMany(Salon::class) -> where(function(Builder $q) {
//            $q -> where(function(Builder $q) {
//                $q -> whereNull('closing_date')
//                    -> whereNull('award_date')
//                    -> whereNull('results_date')
//                    -> where('is_future', true);
//            }) -> orWhere(function (Builder $q) {
//                $q -> where(function (Builder $q) {
//                        $q -> whereNotNull('closing_date')
//                            -> orWhereNotNull('award_date')
//                            -> orWhereNotNull('results_date');
//                    })
//                    -> where(function (Builder $q) {
//                        $q -> where(function (Builder $q) {
//                            $q -> whereNull('closing_date')
//                                -> orWhere('closing_date', '>', now());
//                        }) -> orWhere(function (Builder $q) {
//                            $q -> whereNull('award_date')
//                                -> orWhere('award_date', '>', now());
//                        }) -> orWhere(function (Builder $q) {
//                            $q -> whereNull('results_date')
//                                -> orWhere('results_date', '>', now());
//                        });
//                    });
//            });

            $q -> whereNotIn('salons.id', $this->pastSalonsStats()->get(['salons_stats.id'])->toArray());
        })-> orderBy('closing_date', 'desc');
    }

    public function pastSalons(): Relation {
        return $this -> hasMany(Salon::class) -> where(function(Builder $q) {
//            $q -> where(function(Builder $q) {
//                $q -> whereNull('closing_date')
//                    -> whereNull('award_date')
//                    -> whereNull('results_date')
//                    -> where('is_future', false);
//            }) -> orWhere(function (Builder $q) {
//                $q -> where(function (Builder $q) {
//                    $q -> whereNotNull('closing_date')
//                        -> orWhereNotNull('award_date')
//                        -> orWhereNotNull('results_date');
//                })
//                -> where(function (Builder $q) {
//                    $q -> where(function (Builder $q) {
//                        $q -> whereNull('closing_date')
//                            -> orWhere('closing_date', '<=', now());
//                    }) -> where(function (Builder $q) {
//                        $q -> whereNull('award_date')
//                            -> orWhere('award_date', '<=', now());
//                    }) -> where(function (Builder $q) {
//                        $q -> whereNull('results_date')
//                            -> orWhere('results_date', '<=', now());
//                    });
//                });
//            });
            $q -> whereIn('salons.id', $this->pastSalonsStats()->get(['salons_stats.id'])->toArray());
        })-> orderBy('closing_date', 'desc');
    }

    public function pastSalonsStats(): Relation {
        // Я исправил, потому что в определении salonsStats УЖЕ указано, что WHERE salons.closing_date < DATE(NOW()) OR  salons.closing_date  IS NULL
        return $this -> salonStats();
    }

    public function salonStats(): Relation {
        return $this -> hasMany(SalonsStatsView::class)
            -> orderBy('closing_date', 'desc');
    }

    public function salonStatsAfterParticipation($date): Relation {
        return $this -> hasMany(SalonsStatsView::class)
            -> where('closing_date', '>=', Carbon::parse($date)->toDateTimeString())
            -> orderBy('closing_date', 'desc');
    }

    public function salonStatsBeforeParticipation($date) {
        return $this -> hasMany(SalonsStatsView::class)
            -> where('start_date', '>', Carbon::parse($date)->toDateTimeString())
            -> orderBy('closing_date', 'desc');
    }


    public function columns() {
        return $this -> hasMany(Column::class);
    }

    public function photos() {
        return $this -> hasMany(Photo::class);
    }

    public function photoStats() {
        return $this -> hasMany(PhotoStatsView::class)
            ->orderBy('participation_count', 'desc')
            ->orderBy('acceptances_count', 'desc')
            ->orderBy('awards_count', 'desc');
    }

    public function cancelSubscription() {
        $billingID = $this->subscriptions->billing_id;
        $agreement = new PaypalAgreement();
        $plan = $agreement->getPlanDetails($billingID);
        if($plan->state == 'Active') {
            $agreement->suspend($billingID);
            return $this->subscriptions()->update(['cancel' => true]);
        }
        return false;
    }

    public function getSubscriptionPrice(){
        if($this->subscriptions()->exists()){
            return $this->subscriptions->price;
        }
        return false;
    }

    public function createSubscription($subscription) {
        return $this->subscriptions()->create($subscription);
    }

    public function countryStats() {
        return $this -> hasMany(CountriesStatsView::class)
           // -> orderBy('name')
            -> orderBy('award_count','desc');
    }

    public function isAdmin() {
        return $this->is_admin;
    }

    public function findByCoupon($code) {
        $couponDiscount = $this->vouchers()
            ->withTrashed()
            ->whereHas('users', function($q) use ($code) {
                $q->where('code', $code);
            })->first();

        if($couponDiscount !=null) {
            return $couponDiscount;
        }
        return false;
    }

    public function popularAward(){
    return $this->pastSalons()
            ->join('participations','participations.salon_id','salons.id')
            ->join('acceptances','acceptances.participation_id', 'participations.id')
            ->select(DB::raw('acceptances.award as award, COUNT(*) as award_count'))
            ->groupBy('award')
            ->orderBy('award_count','desc')
            ->where('acceptances.award','!=',null)
            ->get()
            ->toArray();
    }

    public function totalOrganizationSalons(){
    return $this->pastSalons()
            ->join('patronages','patronages.salon_id','salons.id')
            ->select(DB::raw('patronages.organization as organization, COUNT(*) as salon_count'))
            ->groupBy('organization')
            ->orderBy('salon_count','desc')
            ->get()
            ->toArray();
    }

    public function statisticsOfMoney() {
       $data  = [];
       $query = $this->pastSalonsStats()->get();
        $data = [
            'total'      => $query->sum('entry_fee'),
            'unsuccessful_total' =>  $query->where('acceptances_count', '=',0)->sum('entry_fee'),
            'successful' => $query->where('acceptances_count', '!=', 0)->sum('entry_fee'),
        ];
        return $data;
    }

    public function acceptByOrganizations() {
        return $this->pastSalonsStats()
            ->join('patronages' , 'salons_stats.id', '=' ,'patronages.salon_id')
            ->select('patronages.organization', DB::raw('SUM(salons_stats.acceptances_count) AS acceptances_count'))
            ->groupBy('patronages.organization')
            ->get()
            ->toArray();
    }

    public function acceptByOrganizationsAfterParticipation($date) {
        return $this->salonStatsAfterParticipation($date)
            ->join('patronages' , 'salons_stats.id', '=' ,'patronages.salon_id')
            ->select('patronages.organization', DB::raw('SUM(salons_stats.acceptances_count) AS acceptances_count'))
            ->groupBy('patronages.organization')
            ->get()
            ->toArray();
    }

    public function acceptByOrganizationsBeforeParticipation($date) {
        return $this->salonStatsBeforeParticipation($date)
            ->join('patronages' , 'salons_stats.id', '=' ,'patronages.salon_id')
            ->select('patronages.organization', DB::raw('SUM(salons_stats.acceptances_count) AS acceptances_count'))
            ->groupBy('patronages.organization')
            ->get()
            ->toArray();
    }

    public function scopeFilter($query, $params) {
       if(!empty($params)) {
           if ($params['first-name']&& trim($params['first-name']) !== '') {
               $query->where('name', 'LIKE', trim($params['first-name']) . '%');
           }
           if ($params['last-name'] && trim($params['last-name']) !== '') {
               $query->where('surname', 'LIKE', trim($params['last-name']) . '%');
           }
           if ($params['email']     && trim($params['email']) !== '') {
               $query->where('email', 'LIKE', trim($params['email']) . '%');
           }
           if ($params['country']) {
            $query->whereHas('country', function ($query) use ($params) {
                $query->where('countries.name', $params['country']);
            });
        }
       }
        return $query;
    }
}
