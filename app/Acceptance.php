<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Acceptance
 *
 * @property int $id
 * @property string $photo_title
 * @property int $user_id
 * @property int $country_id
 * @property int $salon_id
 * @property string $award
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance whereAward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance wherePhotoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance whereSalonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance whereUserId($value)
 * @mixin \Eloquent
 * @property int $participation_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Acceptance whereParticipationId($value)
 */
class Acceptance extends Model
{
    protected $fillable = ['participation_id', 'award'];

    public function setPhotoTitleAttribute($title){
        $this->attributes['photo_title'] = strtolower($title);
    }

    public function user() {
        return $this -> salon() -> user;
    }

    public function salon() {
        return $this -> participation() -> salon;
    }

    public function country() {
        return $this -> belongsTo(Country::class);
    }

    public function participation() {
        return $this -> belongsTo(Participation::class);
    }

    public function columnsvalue() {
        return $this -> belongsTo(ColumnsValues::class);
    }
}
