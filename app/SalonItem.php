<?php


namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Facades\Auth;

/**
 * @noinspection PhpTooManyParametersInspection
 * Trait SalonItem
 * @package App
 *
 * @method HasMany patronages
 * @method HasManyThrough hasManyThrough($a, $b, $c, $d, $e, $f)
 */
trait SalonItem
{
    public function getPatronagesFormattedAttribute() {
        return $this->patronages()->get()->map(function(Patronage $patronage) {
            return "$patronage->organization ($patronage->number)";
        })->implode(', ');
    }

    public function photoStats() {
        return $this -> hasManyThrough(PhotoStatsView::class, Participation::class, 'salon_id', 'title', 'id','photo_title')
            ->where('photo_stats.user_id', Auth::id())->distinct();
    }

    public function photos() {
        return $this -> hasManyThrough(Photo::class, Participation::class, 'salon_id', 'title', 'id','photo_title')
            ->where('photos.user_id', Auth::id())->distinct();
    }


}