<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AccountSuspended::class,
        Commands\CloseSalon::class,
        Commands\PaymentReminder::class,
        Commands\SalonAward::class,
        Commands\SalonResult::class,
        Commands\UpcomingSalon::class,


    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('salon:close')->daily()->evenInMaintenanceMode();
        $schedule->command('payment:reminder')->daily()->evenInMaintenanceMode();
        $schedule->command('upcoming:salon')->daily()->evenInMaintenanceMode();
        $schedule->command('account:suspended')->daily()->evenInMaintenanceMode();
        $schedule->command('salon:award')->daily()->evenInMaintenanceMode();
        $schedule->command('salon:result')->daily()->evenInMaintenanceMode();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
