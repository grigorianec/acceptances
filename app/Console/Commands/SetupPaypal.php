<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SetupPaypal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paypal:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set up paypal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this -> createProduct();
        $this -> createPlans();
    }
}
