<?php

namespace App\Console\Commands;

use App\Mail\EmailWithCloseSalon;
use App\Salon;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CloseSalon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'salon:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending mail to the user when the salon closes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $salons = Salon::getSalonWitchNotifications('closing_date', 'closing_date' );
        foreach ($salons as $salon) {
              Mail::to($salon->user)->send(new EmailWithCloseSalon($salon->name, $salon->closing_date));
        }
        $this->info('Messages sent successfully!');
    }
}
