<?php

namespace App\Console\Commands;

use App\Mail\EmailAccountSuspended;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class AccountSuspended extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:suspended';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron is working fine!");
      //  $users = User::first();
        $users = User::whereDate('paid_until','=', now())
            ->get();

        foreach ($users as $user) {
            $user->subscriptions()->update(['cancel' => true]);
        }
        Mail::to($users)->send(new EmailAccountSuspended());
        $this->info('Messages sent successfully!');
    }
}

