<?php

namespace App\Console\Commands;

use App\Mail\EmailWithExpiringPaymend;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class PaymentReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a Daily email to all users who ends up  Payment expiring in 7 day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereDate('paid_until','>=', now())
            ->whereDate( 'paid_until', '=', now()->addDays(7) )
            ->get();
        Mail::to($users)->send(new EmailWithExpiringPaymend(now()->addDays(7)));
        $this->info('Messages sent successfully!');
    }
}
