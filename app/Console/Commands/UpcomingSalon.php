<?php

namespace App\Console\Commands;

use App\Mail\EmailUpcomingSalon;
use App\Salon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class UpcomingSalon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upcoming:salon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $salons = Salon::with('user')->whereDate('start_date','>=', now())
            ->whereDate( 'start_date', '=', now()->addDays(1) )
            ->get();

        foreach ($salons as $salon) {
            Mail::to($salon->user)->send(new EmailUpcomingSalon($salon->name, $salon->start_date));
        }
          $this->info('Messages sent successfully!');
    }
}
