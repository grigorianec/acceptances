<?php

namespace App\Console\Commands;

use App\Mail\EmailSalonResultDate;
use App\Mail\EmailWithCloseSalon;
use App\Salon;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SalonResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'salon:result';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $salons =  Salon::getSalonWitchNotifications('results_date', 'results_date' );
        foreach ($salons as $salon) {
            Mail::to($salon->user)->send(new EmailSalonResultDate($salon->name, $salon->results_date));
        }
        $this->info('Messages sent successfully!');
    }
}
