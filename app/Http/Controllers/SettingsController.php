<?php

namespace App\Http\Controllers;

use App\Country;
use App\Exceptions\UserFriendlyException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    use Traits\DataExport;

    public function __invoke ()
    {
        $user = Auth::user();
        return view('settings', [
            's_name' => $user->name,
            's_surname' => $user->surname,
            'country' => $user->country()->first()->id ?? NULL,
            'countries' => Country::assoc(),
        ]);
    }


    public function profileData() {

        $user = Auth::user();
        return view('personal-details', [
            's_name' => $user->name,
            's_surname' => $user->surname,
            'country' => $user->country()->first()->id ?? NULL,
            'countries' => Country::assoc(),
        ]);
    }

    public function payment()
    {

        $user = Auth::user();
        /** @var Carbon $date */
        $date = $user -> paid_until;

        return view('settings-payment', [
            'days_remaining' => $date -> diffInDays(now()),
            'plan' => Config::get('payments.plans'),
            'isTrial' => !$user->subscriptions()->exists(),
            'price'   =>  $user->getSubscriptionPrice(),
        ]);
    }


    public function submitProfileData(Request $request) {
        $this -> savePersonalDetails($request);
        return redirect(route('upload.acceptances'));
    }



    public function account()
    {
        $user = Auth::user();
        return view('settings-account', [
            's_email' => $user->email
        ]);
    }


    public function data() {
        return view('settings-data');
    }


    private function savePersonalDetails(Request $request) {

        $user = Auth::user();
        foreach (['name', 'surname', 'country_id', 'email'] as $field)
            if ($request->$field && $request->$field != $user->$field)
                $user->$field = $field == 'country_id' ? ((int) $request->$field) : $request->$field;

        if ($request->new_password) {
            if (!$request->password)
                throw new UserFriendlyException('Please input password');
            if ($request->new_password != $request->confirm_password)
                throw new UserFriendlyException('Passwords don`t match');
            if (Hash::make($request->password) !== $user->password)
                throw new UserFriendlyException('Wrong password');
            $user->password = Hash::make($request->new_password);
        }

        $user->save();
    }

    public function submit(Request $request)
    {
        $this -> savePersonalDetails($request);
        return redirect()->back();
    }



    public function dropAccount()
    {
        try {
            Auth::user()->delete();
            if(Auth::user()->subscriptions()->exists()) {
                Auth::user()->cancelSubscription();
            }
        } catch (\Exception $e) {}
        return redirect(route('profile'));
    }
}
