<?php

namespace App\Http\Controllers\Admin;

use App\Mail\EmailAccountSuspended;
use App\Mail\EmailUpcomingSalon;
use App\Mail\EmailWithCloseSalon;
use App\Mail\EmailWithExpiringPaymend;
use App\Paypal\PaypalAgreement;
use App\Salon;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;

class UsersController extends Controller
{

    public function index() {
       $params = \request()->all();
       $users = User::filter($params)->with('country')->where('is_admin', '!=' ,true)->get()->toArray();
       return view('users', compact('users', 'params'));
    }

    public function profile($id) {
      $user = User::findOrFail($id);
      return view('user-profile',compact('user'));
    }

    public function enterUserProfile($id) {
        session()->put('adminID', Auth::id());
        Auth::logout(); // for end current session
        Auth::loginUsingId($id);
        return redirect()->route('profile');

    }

    public function editPayment(Request $request) {
        if($request->has('from-date')) {
            // Form validation
            $validator = Validator::make($request->all(), [
                'from-date' => 'required|date|date_format:Y-m-d|after:tomorrow',
            ]);

            if ($validator->passes()) {
                $user = User::findOrFail($request->get('edit-payment-period-id'));
                $user->paid_until = $request->get('from-date');
                $user->save();
                return redirect()->back();
            }
              return redirect()->back();
        }

    }

    public function destroy($id) {
      $user = User::findOrFail($id);
        if($user->subscriptions()->exists()) {
             $user->cancelSubscription();
        }
      $user->delete();
      return redirect()->route('admin.users');
    }
}
