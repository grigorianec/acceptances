<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Discount\DiscountCreateRequest;
use App\Voucher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscountsController extends Controller
{


    public function index() {
        $discountsActive    = Voucher::discountsActive();
        $discountsNotActive = Voucher::discountsNotActive();
        return view('discounts',compact('discountsActive', 'discountsNotActive'));
    }

    public function create() {
        return view('add-discount');
    }

    public function store(DiscountCreateRequest $request) {
     Voucher::createDiscount($request->except('_token','submit'));
     return redirect()->route('admin.discounts');
    }

    public function destroy( Request $request) {
       $coupon = Voucher::findOrfail($request->delete_discount_id);
       $coupon->delete();
       return redirect()->back();
    }
}
