<?php

namespace App\Http\Controllers;

use App\Patronage;
use App\Photo;
use App\Salon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GenericSalonsController extends Controller
{
    protected const PER_PAGE = 10;

    public function delete(int $salonId) {
        try {
            $item = Salon::findOrFail($salonId);
            $this->authorize('modify', $item);

            foreach($item->photos as $photo) {
                /** @var Photo $photo*/
                foreach($photo->participations as $participation) {
                    if ($participation -> salon_id != $salonId)
                        continue 2;
                }
                $photo -> delete();
            }

            $item->delete();
        } catch (\Exception $e) {}
        return redirect() -> route('future-salons');
    }

    public function createSalon(Request $request, bool $future = false) {
        $salonId = Salon::create([
            'name' => $request -> name,
            'user_id' => Auth::user() -> id, 
            'is_future' => $future
        ]) -> id;
        foreach (array_combine($request -> organizations, $request -> numbers) as $organization => $number) {
            Patronage::create(array_merge(compact('organization', 'number'), [
                'salon_id' => $salonId,
            ]));
        }
        return $salonId;
    }

    protected function modifyPatronages(Request $request) {
        if ($o = $request -> organizations) {
            foreach (array_keys($o) as $id) {
                $patronage = Patronage::find($id);
                if ($patronage) {
                    $patronage -> organization = $o[$id];
                    $patronage -> number = $request -> numbers[$id];
                    $patronage -> save();
                }
            }
        }
        if ($d = $request -> deletable) {
            foreach (json_decode($d, TRUE) as $id) {
                $patronage = Patronage::find($id);
                if ($patronage)
                    try {
                        $patronage->delete();
                    } catch (\Exception $e) {}
            }
        }
    }

    protected function modifySalon(array $fields, int $salonId, Request $request): Salon {
        $item = Salon::findOrFail($salonId);
        $this->authorize('modify', $item);

        $item->checkedBestAuthor($request->get('best_author'));
        $item->checkedAwardWereDelivered($request->get('award_were_delivered'));
        foreach ($fields as $field)
            if ($request->$field)
                $item -> $field = $request -> $field;
        $item -> save();

        $this -> modifyPatronages($request);
        if ($request -> _organizations) {
            foreach (array_combine($request -> _organizations, $request -> _numbers) as $organization => $number) {
                Patronage::create(array_merge(compact('organization', 'number'), [
                    'salon_id' => $salonId,
                ]));
            }
        }
        return $item;
    }

}
