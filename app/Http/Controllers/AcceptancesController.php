<?php

namespace App\Http\Controllers;

use App\Acceptance;
use App\Column;
use App\Country;
use App\Salon;
use App\Services\AcceptanceSaveRecordService;
use App\Services\AcceptanceService;
use App\Services\UploadService;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;


class AcceptancesController extends Controller
{

    /**
     * @var AcceptanceService
     */
    private $service;
    /**
     * @var AcceptanceSaveRecordService
     */
    private $saveRecordService;
    /**
     * @var UploadService
     */
    private $uploadService;

    public function __construct(
        AcceptanceService $service,
        AcceptanceSaveRecordService $saveRecordService,
        UploadService $uploadService)
    {
        $this->service = $service;
        $this->saveRecordService = $saveRecordService;
        $this->uploadService = $uploadService;
    }

    public function __invoke (Request $request)
    {

        $request->flash();
        $old = $request->toArray();
        $types = $this->service->acceptanceTypes();

        list($relationship, $names) = $this->service->getRelationship();
        list($columns, $newNames) = $this->service->getNames();

        $builder = Auth::user() -> pastSalonsStats();

        // $builder = Auth::user() -> pastSalons();
        $builder = $this->service->applySalonsFilters($builder, $request);
        $data = $this->service->formatData($builder -> getQuery() -> get(), $relationship);
        $count = count($data);
        $countries = Country::assoc();
        $salons = Auth::user() -> pastSalons() -> get(['id', 'name']);
//        if (count($salons) == 0){
//            return redirect('/app/acceptances/upload-accept');
//        }
//        dd($data);
        return view('my-acceptances', compact('types', 'names', 'data', 'countries', 'count', 'old', 'salons','newNames'));
    }

    public function acceptUpload (Request $request)
    {

        $request->flash();
        $old = $request->toArray();
        $types = $this->service->acceptanceTypes();

        list($relationship, $names) = $this->service->getRelationship();
        list($columns, $newNames) = $this->service->getNames();

        $builder = Auth::user() -> pastSalons();

        $builder = $this->service->applySalonsFilters($builder, $request);
        $data = $this->service->formatData($builder -> getQuery() -> get(), $relationship);
        $count = count($data);
        $countries = Country::assoc();
        $salons = Salon::where('user_id', Auth::user()->id) -> get(['id', 'name']);

        return view('upload', compact('types', 'names', 'data', 'countries', 'count', 'old', 'salons','newNames'));
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function upload(Request $request)
    {
        $userColumns = $this->uploadService->getUserColumns();
        list($analyzedColumns, $analyzedData, $acceptancesCount)
            = $this->uploadService->getFoundColumns($request);
        $countColumns = count($userColumns);
        return view('check-acceptances', compact('userColumns', 'analyzedColumns', 'analyzedData','countColumns', 'acceptancesCount'));
    }

    public function saveUploadedAccepts(Request $request)
    {
        $data = $this->uploadService->handleColumnsOrder($request);
        foreach ($data as $record){
            $salon = $this->saveRecordService->createSalonFromImport($record);
            $participation = $this->saveRecordService->createParticipation($record, $salon);
            $acceptance = $this->saveRecordService->createAcceptance($record,$participation);
            $this->saveRecordService->saveColumnValues($record,$acceptance);
            $this->saveRecordService->addPatronagesRecord($record['acceptance-patronages'], $salon);
        }
        return redirect()->route('acceptances');
    }

    public function addColumn(Request $request)
    {
        $user = Auth::user();
        Column::create(
            [
                'user_id' => $user->id,
                'name'  => $request['add-column-name'],
            ]
        );

        return redirect()->route('acceptances');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function addAccept(Request $request)
    {
        $salon = Salon::find($request['values']['acceptance-salon-id']);
        if ($salon)
            $this->authorize('modify', Salon::findOrFail($request['values']['acceptance-salon-id']));
        $salon = $this->saveRecordService->createSalon($request['values']);
        $participation = $this->saveRecordService->createParticipation($request['values'], $salon);
        $acceptance = $this->saveRecordService->createAcceptance($request['values'],$participation);
        $this->saveRecordService->saveColumnValues($request['values'],$acceptance);
        $this->saveRecordService->addPatronagesRecord($request['values']['acceptance-patronages'], $salon);

        return response()->json([
                'salon_id' => $salon->id,
                'acceptance_id' => $acceptance->id
            ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function updateAccept(Request $request)
    {
        $acceptance = $this->saveRecordService->updateAcceptance($request['values']);
        $participation = $this->saveRecordService->updateParticipation($request['values'], $acceptance);
        $salon = $this->saveRecordService->updateSalon($request['values'],$participation);
        $this->saveRecordService->updateColumnValues($request['values'],$acceptance);
        $this->saveRecordService->updatePatronagesRecord($request['values']['acceptance-patronages'], $salon);

        return response()->json([
            'salon_id' => $salon->id,
            'acceptance_id' => $acceptance->id
        ]);

    }

    public function updateCustomColumn(Request $request)
    {
        if ($name = $request->get('edit-column-name')){
            // TODO add policy for columns
            $column = Column::where('id', $request->get('edit-column-id'));
            $column->update([
                'name' => $name
            ]);
        }
        return redirect()->route('acceptances');
    }

    public function deleteCustomColumn(Request $request)
    {
        if ($id = $request->get('delete-column-id')){
            $column = Column::where('id', $id);
            $column->delete();
        }

        return redirect()->route('acceptances');
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws Exception
     */
    public function destroyAccept($id)
    {
        $acceptance = Acceptance::find($id);
        $this->authorize('destroyAccept', $acceptance);
        $this->saveRecordService->checkLastPhoto($id);
        $participation = $acceptance -> participation();
        Acceptance::destroy($id);
        $participation -> delete();
        return response()->json(['success' => 'success']);
    }

}
