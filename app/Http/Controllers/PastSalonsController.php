<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Country;
use App\Http\Resources\PastSalonResource;
use App\Salon;
use App\SalonsStatsView;
use App\Services\FilterService;
use App\Traits\UploadTrait;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\HttpException;
use ZipArchive;

class PastSalonsController extends GenericSalonsController
{

    use UploadTrait;

    private $service;
    /**
     * @var Request
     */
    private $request;

    public function __construct(FilterService $service, Request $request)
    {
        $this->service = $service;
        $this->request = $request;
    }

    public function __invoke ($page = 1)
    {
        $builder = Auth::user() -> pastSalonsStats();
        if (!empty($params = $this->request->all())){
            $builder = $this->service->salonsData($builder, $this->request);
        }

        $salons = $builder
            ->offset(self::PER_PAGE*($page-1))
            ->take(self::PER_PAGE)
            ->get();
//        if (empty($this->request->all()) && count($salons) == 0)
//            return redirect('/app/acceptances/upload-accept');

        $count = ceil(Auth::user()->pastSalons() -> count()/self::PER_PAGE);
        if ($page > $count && $page != 1)
            throw new HttpException(404);

        return view('past-salons', compact('salons', 'page', 'count', 'params'));
    }

    public function add () {
        $formAction = route('past-salons.create');
        return view('add-past-salon', compact('formAction'));
    }

    public function api(){
        try {
            $user = Auth::user();
            if (!$user)
                throw new \Exception();
             PastSalonResource::withoutWrapping();
             return PastSalonResource::collection($user->pastSalons()->with('patronages')->get());
        } catch (\Exception $e) {
            return ['error' => 'User not found'];
        }
    }

    public function item ($salonId) {
        $item = SalonsStatsView::findOrFail($salonId);
        $salon = Salon::with('acceptances','photoStats')->findOrFail($salonId);
        $this->authorize('modify', $salon);

        $salonAward = trim(implode(',',$salon->acceptances->unique('award')->pluck('award')->toArray()),',');
        $salon -> viewed = true;
        $salon -> save();
        return view('past-salon', compact('item','salon','salonAward'));
    }

    /**
     * @param $salonId
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit ($salonId)
    {
        $item = Salon::findOrFail($salonId);
        $this->authorize('modify', $item);
        $formAction = route('past-salons.modify', ['salonId' => $item -> id]);
        return view('add-past-salon', compact('item', 'formAction'));
    }

    /**
     * @param $salonId
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function editDetails ($salonId)
    {
        $item = Salon::findOrFail($salonId);
        $this->authorize('modify', $item);
        $countries = Country::assoc();
        return view('add-past-salon-2', compact('item', 'countries'));
    }

    /**
     * @param $salonId
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function editMaterials ($salonId)
    {
        $item = Salon::findOrFail($salonId);
        $this->authorize('modify', $item);
        return view('add-past-salon-3', compact('item'));
    }


    public function create(Request $request) {
        $salonId = $this -> createSalon($request);
        return redirect(route('past-salons.edit-details', compact('salonId')));
    }

    /**
     * @param string $salonId
     * @param Request $request
     * @return RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function modify(string $salonId, Request $request) {
        $item = $this -> modifySalon(
            ['name', 'country_id', 'closing_date', 'entry_fee','catalogue_link',
                'comment', /*'best_author',*/  'tracking_number', /* 'awards_received' */],
            $salonId, $request
        );
        if ($request -> name && !$item -> viewed)
            return redirect(route('past-salons.edit-details', compact('salonId')));
        if ($request -> step && !$item -> viewed)
            return redirect(route('past-salons.edit-materials', compact('salonId')));
        return redirect(route('past-salons.item', compact('salonId')));
    }

    public function uploadAttachment(Request $request, $id) {
        $files =[];
        if ($request->file('report')) $files['REPORT_CARD'] = $request->file('report');
        if ($request->file('acceptances')) $files['ACCEPTANCE_LIST'] = $request->file('acceptances');
        if ($request->file('award')) $files['AWARDS_LIST'] = $request->file('award');

      foreach ($files as $type=> $file) {
          $salon = Salon::findOrFail($id);
          $folder = '/attachments/file/';
          $name = time().$salon->id.'_'.$file->getClientOriginalName();
          $filePath = $folder . $name. '.' . $file->getClientOriginalExtension();
          // Upload file
          $this->uploadOne($file, $folder, 'public', $name);

          $salon->attachments()->save(new Attachment([
              'title'=> 'Attachment  '.ucfirst(str_replace('_', ' ',strtolower($type))),
              'file'  => $filePath,
              'type'  => $type,
          ]));
      }

        return redirect()->route('past-salons.item', $id);

    }

    public function downloadAttachment($type, $id) {
        $files = Attachment::getAttachmentsSalon($id, $type);

        if(!$files->isEmpty()) {
            $path = "download";
            $dir = public_path($path);
            if (!File::isDirectory($dir)) {
                Storage::disk('public')->makeDirectory($path, 0755, true, true);
            }

            foreach ($files as $file) {
                if(Storage::disk('public')->exists($file->file)) {
                    $zipPatch = public_path() . '/download/attachments.zip';
                    $zip = new ZipArchive;
                    $zip->open($zipPatch, ZipArchive::CREATE | ZipArchive::OVERWRITE);
                    $zip->addFile(public_path() . $file->file, basename($file->file));
                }else {
                    return redirect()->back();
                }
            }
            $zip->close();
            return response()->download($zipPatch);
        }
        return redirect()->back();
    }

    /**
     * @param int $salonId
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function delete(int $salonId)
    {
        parent::delete($salonId); // TODO: Change the autogenerated stub
        return redirect() -> route('past-salons');
    }


}
