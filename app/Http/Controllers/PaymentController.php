<?php

namespace App\Http\Controllers;

use App\Paypal\CreatePayment;
use App\Paypal\CreatePlan;
use App\Paypal\ExecutePayment;
use App\Paypal\PaypalAgreement;
use App\Traits\Plan;
use App\User;
use App\Voucher;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use PayPal\Api\Agreement;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Common\PayPalModel;


class PaymentController extends Controller
{

    use Plan;
    const PLANS = [
        1 => 9
    ];

    public function renewal()
    {
        $name = Auth::user() -> name;

        $months =  ['1','3','12'];
        return view('renewal', compact('name','months'));
    }

    public function __invoke (string $months)
    {
        $defaultPrice = 0;
        foreach (config('payments.plans') as $plan)
            if ($plan['months'] == $months)
                $defaultPrice = $plan['price'];
        return view('payment', compact('months', 'defaultPrice'));
    }

    public function pay(Request $request) {
      //  dd($request->all());
        $coupon = Auth::user()->findByCoupon($request->discount);
        /**return the optional plan by the user*/
        $subscriberPlans = $this->getPlan($request);
        Session::put('months', $subscriberPlans['months']);

        if($coupon) {
            $price = discount($coupon, $subscriberPlans);
          } else {
            $price = $subscriberPlans['price'];

          }
        Session::put('price', $price);
        /**Create a new billing plan,  return plan */
        $plan   = new CreatePlan($subscriberPlans['months'], $subscriberPlans['description'], $price);
        $planId = $plan->createPlan();

        $agreement = new PaypalAgreement();
        return $agreement->paypalRedirect($planId->getId());
    }

    public function execute($status) {
        if($status == 'true') {
            // Execute agreement
            $agreement = new PaypalAgreement();
            $billingID = $agreement->executeAgreement(\request('token'));

            $months = Session::get('months');
            $price  = Session::get('price');
            $user   = User::findOrFail(Auth::id());
            $user->paid_until = now()->addMonths($months);
            $user->save();

            // create billing_id
            $user->createSubscription(['billing_id' => $billingID,'price' => $price]);
            return redirect()->route('profile');
        }
    }

    public function cancel() {
        if(Auth::user()->subscriptions()->exists()) {
          Auth::user()->cancelSubscription();
        }
        return redirect()->route('settings.payment');
    }
}
