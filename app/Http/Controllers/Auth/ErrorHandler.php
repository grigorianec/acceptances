<?php


namespace App\Http\Controllers\Auth;


use Exception;
use Illuminate\Validation\ValidationException;

trait ErrorHandler
{
    protected function handleValidationException(ValidationException $e) {
        $errors = $e->validator->errors()->all();
        if (is_array($errors))
            $err = implode("\n", array_values($errors));
        else
            $err = $errors;
        return view('login', ['error' => $err]);
//        return view('error', ['n' => 500, 'e' => new Exception($err)]);
    }
}