<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LoginController extends Controller
{
    use ErrorHandler;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        sendFailedLoginResponse as failedResponse;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/app/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        if ( $user->isAdmin() ) {
            return redirect('/app/admin');
        }

        return redirect('/app');
    }



    public function showLoginForm ()
    {
        return view('login');
    }

    protected function sendFailedLoginResponse (Request $request)
    {
        try {
            return self::failedResponse($request);
        } catch (ValidationException $e) {
            return $this->handleValidationException($e);
        }
    }

    public function logout (Request $request)
    {
        if(session()->has('adminID')) {
            $this->logoutSpecificUser();
            return redirect()->route('admin.users');

        }

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect(route('login'));
    }

   // add vendor/laravel/framework/src/illuminate/foundation/auth/authenticatesUser
    public function logoutSpecificUser() {
        $this->guard()->logout();
        $adminId = session()->get('adminID');
        Auth::loginUsingId($adminId);
        session()->forget('adminID');
    }


}
