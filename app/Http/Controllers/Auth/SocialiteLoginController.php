<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SocialiteLoginController extends Controller
{

    /**
     * Redirect the user to Socialite authentication page.
     *
     * @param $service
     * @return RedirectResponse
     */
    public function redirect($service)
    {

        return Socialite::driver($service)->redirect();
    }

    /**
     * Obtain the user information from Socialite.
     *
     * @param $service
     * @return RedirectResponse
     */
    public function callback($service)
    {

        $token = $service.'_token';

        try {
            $user = Socialite::driver($service)->user();
        }
        catch (\Exception $e) {
            return redirect(route('login'));
        }

        $existingUser = User::where($token, $user->getId())->first();

        if($existingUser){
            Auth::loginUsingId($existingUser->id, true);
            return redirect(route('profile'));

        } else {

            $emailUser = User::where('email', $user->getEmail())->first();

            if ($emailUser) {

                $emailUser->email = $user->getEmail();
                $emailUser->save();

                Auth::loginUsingId($emailUser->id, true);
                return redirect(route('profile'));

            } else {

                $this -> createUser($user, $token);
                return redirect(route('personal-details'));
            }
        }
    }


    private function createUser(\Laravel\Socialite\Contracts\User $user, string $token) {
        $newUser = new User();

        $name = $user->getName();
        $fullName = explode(' ', $name, 2);
        if (count($fullName) == 2) {
            $newUser->name = $fullName[0];
            $newUser->surname = $fullName[1];
        } else {
            $newUser->name = $name;
        }
        $newUser->email = $user->getEmail();
        $newUser->$token = $user->getId();
        $newUser->password = Hash::make($user->getId());
        $newUser->email_verified_at = Carbon::now();
        $newUser->paid_until = now() -> addMonth();

        $newUser->save();

        Auth::loginUsingId($newUser->id, true);
    }
}
