<?php

namespace App\Http\Controllers;

use App\Acceptance;
use App\CountriesStatsView;
use App\Country;
use App\Participation;
use App\Salon;
use App\SalonsStatsView;
use App\Services\StatisticService;
use App\User;
use App\UsersStatsView;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class StatisticsController extends Controller
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var StatisticService
     */
    private $service;

    public function __construct(Request $request)
    {

        $this->request = $request;
      //  $this->service = $service;
    }

    public function __invoke ()
    {

//        $tt = Auth::user()->pastSalonsStats()
//            ->join('patronages' , 'salons_stats.id', '=' ,'patronages.salon_id')
//            ->select('patronages.organization', DB::raw('SUM(salons_stats.acceptances_count) AS acceptances_count'))
//            ->groupBy('patronages.organization')
//            ->get()
//            ->toArray();
//
//
//        dump( $tt);

        $stats = UsersStatsView::whereUserId(Auth::id())->first();
        $popularAward = Auth::user()->popularAward();
        if (!empty($this->request->get('dates-after-date'))){
            $acceptByOrganizations = Auth::user()->acceptByOrganizationsAfterParticipation($this->request->get('dates-after-date'));
        }elseif (!empty($this->request->get('dates-before-date'))){
            $acceptByOrganizations = Auth::user()->acceptByOrganizationsBeforeParticipation($this->request->get('dates-before-date'));
        }else{
            $acceptByOrganizations = Auth::user()->acceptByOrganizations();
        }

//        dd($acceptByOrganizations);
//        if (is_null($stats))
//            return redirect(route('upload.acceptances'));
        return view('statistics', $stats ? $stats->toArray(): [], compact('popularAward','acceptByOrganizations'));
    }

    public function salons() {
        $organizationSalonCount = Auth::user()->totalOrganizationSalons();
        $past_salons = Auth::user()->pastSalonsStats()
            ->take(4)->get()->sortByDesc('acceptances_count');
        return view('statistics-salons', array_merge(
            UsersStatsView::whereUserId(Auth::id())->first()->toArray(),
            compact('past_salons', 'organizationSalonCount')
        ));
    }

    public function money()
    {
        $moneys = Auth::user()->statisticsOfMoney();
        $stats  = UsersStatsView::whereUserId(Auth::id())->first()->toArray();
        return view('statistics-money',array_merge($moneys, $stats));
    }

    public function photos()
    {
        $photos = Auth::user()->photoStats()->get();
        $gallery = $photos->take(6);

        $photos_with_awards = $photos -> where('awards_count', '>', '0') -> count();

        $stats   = UsersStatsView::whereUserId(Auth::id())->first()->toArray();
        return view('statistics-photos', array_merge(
            $stats, compact('gallery', 'photos_with_awards')
        ));
    }

    public function countries() {
       $stats = UsersStatsView::whereUserId(Auth::id())->first()->toArray();
       $countries = CountriesStatsView::whereUserId(Auth::id())->orderBy('salons_count', 'desc')->get()->toArray();
      return view('statistics-countries', $stats, compact('countries'));
    }

}
