<?php


namespace App\Http\Controllers\Traits;


use App\Attachment;
use App\Salon;
use App\Services\AcceptanceService;
use App\User;
use App\UsersStatsView;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use ZipArchive;

trait DataExport
{
    private $dir;
    private $spreadsheet;
    private $files = [];

    protected function getSpreadSheet(): Spreadsheet {
        return $this -> spreadsheet;
    }

    public function export() {


        $user = Auth::user();
        $salons = $user->salons();

        try {
            $this -> spreadsheet = new Spreadsheet();

            $this -> addProfileData($user);
            $this -> addStats($user);
            $this -> addAttachments($user, $salons);
            $this -> addCountriesStats($user);
            $this -> addPhotoStats($user);
         //   $this -> addSalonStats($user);
            $this -> addAcceptances($user);
            return $this -> sendWithAttachments($user);

        } catch (Exception $e) {
            return view('error', [
                'n' => '500',
                'isLoggedIn' => true,
                'e' => "Could not export file: {$e->getMessage()}"
            ]);
        }
    }

    /**
     * @param User $user
     * @return BinaryFileResponse
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    private function sendWithAttachments(User $user) {
        $dir = storage_path("users/$user->id");
        if (!file_exists($dir))
            mkdir($dir, 0777, true);

        $date = date('Y-m-d');
        $filename = "$dir/MySalonResults export User #$user->id $date";

        $writer = new Xlsx($this -> spreadsheet);
        $writer->save($xlsx = "$filename.xlsx");

        $files = array_merge([$xlsx], $this->files);
        $zipName = "$filename.zip";
        $zip = new ZipArchive;
        $zip -> open($zipName, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        foreach ($files as $file) {
            $zip->addFile($file, basename($file));
        }
        $zip->close();

        unlink($xlsx);
        return response()->download($zipName);
//        return response()->download($xlsx);
    }




    /**
     * @param User $user
     * @throws Exception
     */
    private function addCountriesStats(User $user)
    {
        $sheet = ($s = $this -> getSpreadSheet()) -> addSheet(new Worksheet($s, 'Countries'));
        $data = $user -> countryStats() -> get(['name', 'award_count']) -> toArray();
        $this -> addData($sheet, [['NAME', 'AWARD COUNT']]);
        $this -> addData($sheet, $data, 1);
    }


    /**
     * @param User $user
     * @throws Exception
     */
    private function addPhotoStats(User $user)
    {
        $sheet = ($s = $this -> getSpreadSheet()) -> addSheet(new Worksheet($s, 'Photos'));
        $data = $user -> photoStats() -> get([
            'title',
            'image',
            'acceptances_count',
            'awards_count',
            'participation_count'
        ]) -> toArray();

        foreach ($data as &$row) {
            if ($image = $row['image'])
                $this -> addAttachment(public_path($image));
            unset($row['image']);
        }

        $this -> addData($sheet, [['TITLE', 'ACCEPTANCES COUNT', 'AWARD COUNT', 'PARTICIPATION COUNT']]);
        $this -> addData($sheet, $data, 1);
    }


    /**
     * @param User $user
     * @throws Exception
     */
    private function addSalonStats(User $user)
    {

        $sheet = ($s = $this -> getSpreadSheet()) -> addSheet(new Worksheet($s, 'Salons (past + future)'));
        $data = $user -> salonStats() -> with('attachments') -> with('patronages') -> get([
            'title',
            'image',
            'acceptances_count',
            'awards_count',
            'participation_count'
        ]);

        $data[0] -> patronages_formatted;

        // TODO
//        foreach ($data as $row) {
//            if ($attachments = $row['attachments'])
//                foreach ($attachments as $attachment)
//                    $this -> addAttachment($attachment['file']);
//            unset($row['attachments']);
//        }

        $this -> addData($sheet, [['TITLE', 'ACCEPTANCES COUNT', 'AWARD COUNT', 'PARTICIPATION COUNT']]);
        $this -> addData($sheet, $data, 1);
    }


    /**
     * @param User $user
     * @param HasMany $salons
     */
    private function addAttachments(User $user, HasMany $salons)
    {
        $attachments = Attachment::where('salon_id', $salons -> get('id') -> toArray());
    }


    /**
     * @param User $user
     * @throws Exception
     */
    private function addAcceptances(User $user)
    {
        $sheet = ($s = $this -> getSpreadSheet()) -> addSheet(new Worksheet($s, 'Acceptances'));
        $s = new AcceptanceService;
        list($relationship, $names) = $s -> getRelationship();

        $acceptances = $s -> formatData(Salon::where('user_id', Auth::user()->id) -> get(), $relationship);

        $headers = ['ID', 'SALON_ID', 'SALON NAME', 'PATRONAGE(S)', 'COUNTRY', 'IMAGE TITLE', 'AWARD'];
        $headers = array_merge($headers, array_map(function($x) {return "CUSTOM: $x";}, array_slice($names, 7)));

        $this -> addData($sheet, [$headers]);
        $this -> addData($sheet, $acceptances, 1);
    }


    /**
     * @param User $user
     * @throws Exception
     */
    private function addProfileData(User $user) {

        $sheet = $this -> getSpreadSheet() -> getActiveSheet();
        $sheet -> setTitle('Profile Data');

        $attributes = $user -> getAttributes();

        unset($attributes['password']);

        $this -> addAttributes($sheet, $attributes);


    }

    /**
     * @param User $user
     * @throws Exception
     */
    private function addStats(User $user) {
        $sheet = ($s = $this -> getSpreadSheet()) -> addSheet(new Worksheet($s, 'Statistics'));
        $attributes = UsersStatsView::where('user_id', $user -> id) -> firstOrFail() -> getAttributes();
        unset($attributes['user_id']);

        $this -> addAttributes($sheet, $attributes);
    }

    private function addAttributes(Worksheet $sheet, array $attributes) {

        $i = 0;
        $data = [];
        foreach ($attributes as $attribute => $value) {
            $i++;
            $data[] = [$attribute, $value];


        }
        $this -> addData($sheet, [['ITEM', 'VALUE']]);

        $this -> addData($sheet, $data, 1);
    }



    // Spreadsheet functions
    private function addData(Worksheet $sheet, array $data, int $offset = 0) {

        foreach ($data as $row) {
            $offset++;
            $c = 0;

            foreach ($row as $item) {
                $letter = chr(64 + ++$c);
                if ($offset == 1) {
                    $sheet -> getColumnDimension($letter) -> setAutoSize(true);
                }
                $sheet->setCellValue("$letter$offset", $item);
            }
        }
    }

    private function addAttachment(string $file) {
        if(file_exists($file))
            $this -> files[] = $file;
    }


}
