<?php

namespace App\Http\Controllers;

use App\Mail\SupportInquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class SupportController extends Controller
{
    const RECEIVER = 'support@mysalonresults.com';
    const CC = 'internationalsalons@gmail.com';

    public function __invoke ()
    {

        if(request()->has('message'))
        {
            Mail::to(self::RECEIVER)
                ->cc(self::CC)
                ->send(new SupportInquiry(\request()->message));
//            return 'support mail is being sent (apparently)';
        }
        return redirect() -> back();
    }
}
