<?php

namespace App\Http\Controllers;

use App\Participation;
use App\Photo;
use App\PhotoStatsView;
use App\Salon;
use App\Services\FilterService;
use Faker\Provider\Image;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\View\View;
use Response;
use Str;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Validator;

class GalleryController extends Controller
{
    private $service;
    /**
     * @var Request
     */
    private $request;

    public function __construct(FilterService $service, Request $request)
    {
        $this->service = $service;
        $this->request = $request;
    }

    const PER_PAGE = 12;

    public function __invoke ($page = 1)
    {
        $builder =  Auth::user() -> photos();

        if (!empty($this->request->all())){
            $builder = $this->service->filterPhotoData($builder, $this->request);
        }

//        $gallery = $this->salonPictures($builder, self::PER_PAGE*($page-1), self::PER_PAGE);
        $gallery = $builder->skip(self::PER_PAGE*($page-1))->take(self::PER_PAGE) -> get();

//        if (empty($this->request->all()) && count($gallery) == 0)
//            return redirect('/app/acceptances/upload-accept');
        $count = ceil(Auth::user() -> photos() -> count()/self::PER_PAGE);
        if ($page > $count && $page != 1)
            throw new HttpException(404);
        return view('gallery', compact('gallery', 'page', 'count'));
    }

    /**
     * @param $id
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function item($id) {
        $item = PhotoStatsView::where('id', $id)->first();

        $photo = Photo::where('id', $id)->first();
        $this->authorize('modifyViewPhoto', $photo);
        if (!$item)
            throw new NotFoundHttpException();
        $download = $this->service->showDownloadButton($item);
        return view('gallery-photo', compact('item','photo', 'download'));
    }

    public function updateImage(Request $request, $id) {
        if($request->hasFile('image')) {
            // Form validation
            $validator = Validator::make($request->all(), [
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);

            if ($validator->passes()) {
                $photo = Photo::findOrFail($id);
                $this->authorize('modifyViewPhoto', $photo);

                $photo->uploadImage($request->file('image'));
                return redirect()->back();
            }
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }

    public function download($id) {
        $image = Photo::findOrFail($id);
        $directoryPath = public_path().$image->image;
        if(File::exists($directoryPath)){
            return response()->download($directoryPath);
        }
            return redirect()->back();
    }

    public function updateImageTitle(Request $request)
    {
        $photo = Photo::where('id', $request['photo-title-id'])->first();
        $this->authorize('modifyViewPhoto', $photo);
        $checkTitle = Photo::where('title', $request['photo-title-name'])->where('user_id', Auth::id())->get();
        $participations = Auth::user() -> participations() -> where('photo_title', '=', $photo->title)->get();
        if(empty($checkTitle->toArray())){
            foreach ($participations as $participation){
                $participation->update(['photo_title' => $request['photo-title-name']]);
            }
            $photo->update(
                [
                   'title' => $request['photo-title-name']
                ]
            );
            return redirect()->back();
        }else{
            return redirect()->route('gallery-item', ['id' => $photo->id, 'title-check' => true]);
        }
    }

    /**
     * @param Request $request
     * @return Request
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        $photo = Photo::where('id', $request['photo-delete-id'])->first();
        $this->authorize('modifyViewPhoto', $photo);
        $participations = Auth::user() -> participations() -> where('photo_title', $photo->title)->get();
        if (!empty($participations)){
            foreach ($participations as $participation){
                $participation->delete();
            }
        }
        $photo->delete();
        return redirect()->route('gallery');
    }
}
