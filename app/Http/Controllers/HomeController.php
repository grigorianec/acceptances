<?php

namespace App\Http\Controllers;

use App\Mail\EmailAccountSuspended;
use App\Mail\EmailSalonAward;
use App\Mail\EmailSalonResultDate;
use App\Mail\EmailUpcomingSalon;
use App\Mail\EmailWithCloseSalon;
use App\Mail\EmailWithExpiringPaymend;
use App\Salon;
use App\User;
use App\UsersStatsView;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

    public function __invoke ()
      {
        $future_salons = Auth::user()->futureSalons()->take(2)->get();
        $past_salons = Auth::user()->pastSalonsStats()->take(4)->get();
        $gallery = Auth::user()->photoStats()->take(6)->get();
        if (!$name = Auth::user() -> name)
            return redirect(route('personal-details'));

        if (count($future_salons) + count($past_salons) == 0)
            return view('empty', Auth::user()->toArray());
        return view('profile', array_merge(
            UsersStatsView::whereUserId(Auth::id())->first()->toArray(),
            compact('future_salons', 'past_salons', 'gallery')
        ));
    }
}
