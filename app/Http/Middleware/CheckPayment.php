<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPayment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var Carbon $date $date */
        $date = Auth::user() -> paid_until;
        if (Carbon::now() -> greaterThan($date))
            return redirect(route('renewal'));

        return $next($request);
    }
}
