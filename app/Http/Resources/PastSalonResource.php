<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PastSalonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $patronages = [];
        $value = $this->patronages->pluck('number','organization');
        foreach ($value as $organization=> $number) {
            $patronages [] = $organization.' '.'('.$number.')';
        }

        return [
            'id' => $this->id,
            'salon_name' => $this->name,
            'patronages' => implode(', ',$patronages ),
            // 'patronages' => $this -> patronages_formatted
            'country_id' => $this->country_id
        ];
    }
}
