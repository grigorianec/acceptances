<?php

namespace App\Services;

use App\Acceptance;
use App\Country;
use App\Participation;
use App\Photo;
use App\Salon;
use App\SalonsStatsView;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class FilterService
{
    const PER_PAGE = 12;
    const PER_PAGE_SALON = 10;

    public function salonsData(HasMany $builder, Request $request)
    {
        if (!empty($salonName = $request->get('filter-salon-name'))){
            $builder = $builder
                ->where('name', 'like', "%$salonName%");
        }

        if (!empty($request->get('filter-salon-has-acceptances'))){
            $builder = $builder ->has('acceptances');
        }

        if (!empty($request->get('filter-salon-no-acceptances'))){
            $builder = $builder -> doesntHave('acceptances');
        }

        if (!empty($salonCountry = $request->get('filter-country'))){
            $countries = Country::where('name', 'like', "%$salonCountry%")->get(['id'])->toArray();
            $builder = $builder
                ->whereIn('country_id', $countries);
        }

        if (!empty($participatedAfter = $request->get('filter-after-date')) &&
            !empty($participatedBefore = $request->get('filter-before-date'))){
            $builder = $builder
                ->whereBetween('closing_date',array($participatedAfter, $participatedBefore));
        }else{
            if (!empty($participatedAfter = $request->get('filter-after-date'))){
                $builder = $builder
                    ->where('closing_date', '>', $participatedAfter);
            }

            if (!empty($participatedBefore = $request->get('filter-before-date'))){
                $builder = $builder
                    ->where('closing_date', '<', $participatedBefore);
            }
        }

        if (!empty($photoTitle = $request->get('filter-image-title'))){
            $partObject = Auth::user() -> participations() -> where('photo_title','like', "%$photoTitle%")->get(['salon_id'])->toArray();
            $builder = $builder->whereIn('id',  $partObject);
        }

        if (!empty($organization = $request->get('filter-salon-organization'))) {
            $builder = $builder->whereHas('patronages', function ($q) use ($request) {
                $this->applyPatronagesFilters($q, $request);
            });
        }

        if (!empty($salonAward = $request->get('filter-award')
            || !empty($request->get('filter-is-award'))
            || !empty($request->get('filter-no-award')))){
            $builder = $builder -> whereHas('acceptances', function($q) use ($request) {
                $this->applyAcceptanceFilters($q, $request);
            });
        }

        return $builder;
    }

    public function filterPhotoData(HasMany $builder, Request $request)
    {
        $builder = $builder -> whereHas('participations', function(Builder $q) use ($request) {
            if (!empty($salonName = $request->get('filter-salon-name'))){
                $q->whereIn('salon_id', Salon::where('name', 'like', "%$salonName%")->get(['id'])->toArray());
            }

            if (!empty($country = $request->get('filter-country'))){
                $q->whereIn('salon_id', Salon::whereIn('country_id',
                    Country::where('name', 'like', "%$country%")
                        ->get('id')->toArray())->get(['id'])->toArray());
            }

            if (!empty($award = $request->get('filter-award'))){
                $q->whereIn('id', Acceptance::where('award', 'like', "%$award%")->get(['participation_id'])->toArray());
            }

        });
        if (!empty($photoTitle = $request->get('filter-image-title'))){
            $builder = $builder -> where('title','like', "%$photoTitle%");
        }
        return $builder;
    }

    private function applyPatronagesFilters(Builder $q, Request $request)
    {
        $organization = $request->get('filter-salon-organization');
        $q -> where('organization', 'like', "%$organization%");

    }

    private function applyAcceptanceFilters($q, Request $request) {

        if (!empty($salonAward = $request->get('filter-award'))){
            $q -> where('award', 'like', "%$salonAward%");
        }

        if (!empty($request->get('filter-is-award'))){
            $q -> whereNotNull('award')
                -> where('award', '!=', '');
        } elseif (!empty($request->get('filter-no-award'))){
            $q -> where('award',  null)
                -> orWhere('award', '');;
        }
    }


    public function showDownloadButton($item)
    {
        $show = false;
        if ($item['image'] !== null && $item['image'] !== ''){
            $show = true;
        }
        return $show;
    }

}