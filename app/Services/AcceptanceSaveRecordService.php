<?php


namespace App\Services;


use App\Acceptance;
use App\Column;
use App\ColumnsValues;
use App\Country;
use App\Participation;
use App\Patronage;
use App\Photo;
use App\Salon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Str;

class AcceptanceSaveRecordService
{
    /**
     * @param $request
     * @return Salon|Builder|Builder[]|Collection|Model|object|null
     * @throws Exception
     */
    public function createSalon($request)
    {
        $salon = Salon::where('id', $request['acceptance-salon-id'])->first();
        if (empty($salon)){
            return Salon::create(
                [
                    'user_id' => Auth::user()->id,
                    'name' => $request['acceptance-salon-name'],
                    'country_id' => $request['acceptance-country']
                ]);
        }else{
            return $salon;
        }
    }

    public function createSalonFromImport($request)
    {
        $user = Auth::user();
        $numbers = $this->handledRequestPatronages($request['acceptance-patronages']);
        $patrons = [];
        foreach ($numbers as $key => $item){
            $patrons[] = $item[0];
        }
        $salon = $user -> pastSalons() -> where('name', $request['acceptance-salon-name'])->first();
        if (empty($salon)){
            $country = Country::where('name', $request['acceptance-country'])->first();
            return Salon::create(
                [
                    'user_id' => $user->id,
                    'name' => $request['acceptance-salon-name'],
                    'country_id' => $country->id ?? null
                ]);
        }else{
            return $salon;
        }
    }

    public function createParticipation($request, $salon)
    {
        $this->handlePhotoCreate($request);
        return Participation::create(
            [
                'photo_title' => $request['acceptance-image-title'],
                'salon_id' => $salon->id
            ]
        );
    }

    public function createAcceptance($request, $participation)
    {
        return Acceptance::create(
            [
                'participation_id' => $participation->id,
                'award' => $request['acceptance-award']
            ]
        );
    }

    public function saveColumnValues($request, $acceptance)
    {
        $additionalColumns = Column::where('user_id','=', Auth::user()->id)->get(['id'])->toArray();
        $customColumns = [];
        foreach ($request  as $key => $custom){
            if (Str::contains($key, 'custom')){
                $customColumns[] = $custom;
            }
        }
        foreach ($customColumns as $i => $customColumn){
            ColumnsValues::create(
                [
                    'value' => $customColumn ?? '',
                    'acceptance_id' => $acceptance->id,
                    'column_id' => $additionalColumns[$i]['id']
                ]
            );
        }
    }

    public function addPatronagesRecord($patronages, $salon)
    {
        $patron = [];
        $numbers = $this->handledRequestPatronages($patronages);

        foreach($numbers as $key => $patronage){
            $patron['salon_id'] = $salon->id;
            $patron['organization'] = $numbers[$key][0];
            $patron['number'] = str_replace(')', '', str_replace('(', '', $numbers[$key][1]));
            foreach ($salon -> patronages as $pat)
                if ($pat -> organization == $patron['organization'] && $pat -> number = $patron['number'])
                    continue 2;
            Patronage::create($patron);
        }
    }

    private function handledRequestPatronages($patronages)
    {
        $numbers = [];
        $pieces = [];
        $pieces[] = explode(',', $patronages);
        foreach ($pieces as $key => $piece){
            foreach ($piece as $item){
                $prepared = explode(' ', trim($item));
                $last = array_pop($prepared);
                $numbers[] = [
                    implode(' ', $prepared),
                    $last
                ];
            }
        }
        return $numbers;
    }

    public function updateAcceptance($request)
    {
        $acceptance = Acceptance::where('id', $request['acceptance-id'])->first();
        $acceptance->update(
            [
                'award' => $request['acceptance-award']
            ]
        );
        return $acceptance;
    }

    public function updateParticipation($request, $acceptance)
    {
        $this->handlePhotoEdit($request);
        $participation = Participation::where('id', $acceptance->participation_id)->first();
        $participation->update(
            [
                'photo_title' => $request['acceptance-image-title'],
            ]
        );
        return $participation;
    }

    public function updateSalon($request, $participation)
    {
        $salon = null;
        if (isset($request['acceptance-salon-id'])
            && ($salon = Salon::where('id', $request['acceptance-salon-id'])->first())
            && $salon -> name == $request['acceptance-salon-name']) {
            $salon->update(
                [
                    'country_id' => $request['acceptance-country']
                ]
            );
        } else {
            $salon = Salon::create(
                [
                    'user_id' => Auth::user()->id,
                    'name' => $request['acceptance-salon-name'],
                    'country_id' => $request['acceptance-country']
                ]);
        }
        if ($participation->salon_id != $salon->id) {
            $participation->salon_id = $salon->id;
            $participation->save();
        }
        return $salon;
    }

    public function updateColumnValues($request, $acceptance)
    {
        $additionalColumns = Column::where('user_id','=', Auth::user()->id)->get(['id']);
        $k = 0;
        $customColumns = [];
        foreach ($request  as $key => $custom){
            if (Str::contains($key, 'custom')){
                $customColumns[] = $custom;
            }
        }
        foreach ($customColumns as $i => $customColumn){
            $columnValue = ColumnsValues::where('acceptance_id', $acceptance->id)
                ->where('column_id', $additionalColumns[$k]['id'])->first();
            if (!empty($columnValue)){
                $columnValue->update(['value' => $customColumn]);
            }else{
                ColumnsValues::create(
                    [
                        'value' => $customColumn,
                        'acceptance_id' => $acceptance->id,
                        'column_id' => $additionalColumns[$k]['id']
                    ]
                );
            }
            $k++;
        }
    }

    public function updatePatronagesRecord($patronages, $salon)
    {
        $patron = [];
        $numbers = $this->handledRequestPatronages($patronages);
        $patronage = Patronage::where('salon_id',$salon->id)->get();
        if (count($numbers) != count($patronage)){
            Patronage::where('salon_id',$salon->id)->delete();
        }
        foreach($numbers as $key => $item){
            $patron['salon_id'] = $salon->id;
            $patron['organization'] = $numbers[$key][0];
            $patron['number'] = str_replace(')', '', str_replace('(', '', $numbers[$key][1]));
            if (count($numbers) != count($patronage)){
                Patronage::create($patron);
            }else{
                $patronage[$key]->update($patron);
            }
        }
    }

    private function handlePhotoCreate($request)
    {
        if ($request['acceptance-image-title'] != null || $request['acceptance-image-title'] != ""){
            $photo = Photo::where('title', $request['acceptance-image-title'])->where('user_id', Auth::id())->first();
            if (empty($photo)) {
                Photo::create(
                    [
                        'user_id' => Auth::user()->id,
                        'title' => $request['acceptance-image-title'],
                    ]);
            }
        }
    }

    private function handlePhotoEdit($request)
    {
        if ($request['acceptance-image-title'] != null || $request['acceptance-image-title'] != ""){
            $acceptance = Acceptance::where('id', $request['acceptance-id'])->first();
            $participation = Participation::where('id', $acceptance->participation_id)->first();

            // Because what if two users have the same image title?
            $quantityAccepts = Auth::user() -> participations() -> where('photo_title', $participation->photo_title)->get();
            // If there's just one acceptance with this photo, rename the photo
            if (count($quantityAccepts) === 1){
                $redundantPhoto = Photo::where('title', $participation->photo_title)->where('user_id', Auth::id())->first();

                // If the photo with such name already exists, merge the photos
                if ($existing = Photo::where('title', $request['acceptance-image-title'])->where('user_id', Auth::id())->first())
                    // We delete the photo from this participation because there's just one photo with this photo title and now it's useless.
                    // TODO warn the user about this behaviour
                    if ($redundantPhoto)
                        // We check if photo exists, just in case
                        $redundantPhoto -> delete();

                // We check if photo exists, just in case
                if ($redundantPhoto)
                    $redundantPhoto->update(['title' => $request['acceptance-image-title']]);

            // Else create a new photo
            }elseif (count($quantityAccepts) > 1){
                $photo = Photo::where('title', $request['acceptance-image-title'])->where('user_id', Auth::id())->first();
                if (empty($photo)) {
                    Photo::create(
                        [
                            'user_id' => Auth::user()->id,
                            'title' => $request['acceptance-image-title'],
                        ]);
                }
            }
            $participation->update(['photo_title' => $request['acceptance-image-title']]);
        }
    }

    /**
     * @param $id
     * @throws Exception
     */
    public function checkLastPhoto($id)
    {
        $acceptance = Acceptance::where('id', $id)->first();
        $participation = $acceptance->participation;
        $quantityAccepts = Acceptance::whereIn('participation_id',
            Auth::user() -> participations() -> where('photo_title', $participation->photo_title)->get(['participations.id'])->toArray()
        )->get()->toArray();
        if (count($quantityAccepts) === 1){
            $photo = Photo::where('title', $participation->photo_title)->where('user_id', Auth::id())->first();
            if ($photo)
                $photo->delete();
        }
    }
}