<?php


namespace App\Services;

use App\Column;
use Auth;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Session;

class UploadService
{
    private function requiredColumns()
    {
        return [
            [
                'id' => 'Salon-name',
                'name' => 'Salon name'
            ],
            [
                'id' => 'Patronages',
                'name' => 'Patronages'
            ],
            [
                'id' => 'Country',
                'name' => 'Country'
            ],
            [
                'id' => 'Image-title',
                'name' => 'Image title'
            ],
            [
                'id' => 'Award',
                'name' => 'Award'
            ]
        ];
    }

    public function getUserColumns()
    {
        $userColumns = $this->requiredColumns();

        list($additionalColumns, $customColumnsName) = $this->additionalColumns();
        if (!empty($additionalColumns)){
            foreach ($additionalColumns as $column){
                $userColumns[] = [
                    'id' => $column['name'],
                    'name' => $column['name']
                ];
            }
        }

        return $userColumns;
    }

    private function additionalColumns()
    {
        $additionalColumns = Column::where('user_id', Auth::user()->id)->get('name')->toArray();
        $customColumnsName = [];
        foreach ($additionalColumns as $name){
            $customColumnsName[] = $name['name'];
        }

        return [$additionalColumns, $customColumnsName];
    }

    private function getPatternReplacements()
    {
        $patterns  = [
            '/\bsalon[-,_, ]?[name]?[s]?/i',
            '/\bp[ae]tronage[s]?/i',
            '/\b[c,k]ountr[yi]/i',
            '/\b[i,f,p][m,h,o][a,o,t][g,t,o]?[e,o][-,_, ]title[s]?/i',
            '/\b[a,e]ward[s]?/i'
        ];

        $replacements = [
            'Salon-name',
            'Patronages',
            'Country',
            'Image-title',
            'Award'
        ];

        return [$patterns, $replacements];
    }

    private function getPatronagesColumn($sheetsArray, $pattern, $reformatPatronages = [])
    {
        $patronageTitlesKeys = [];

        foreach ($sheetsArray as $key => $value){
            $data = [];
            if ($key === 0){
                foreach ($value as $i => $item) {
                    if (preg_match($pattern, $item)){
                        // Так захотели =(
//                        $patronage = explode(' ', $item);
//                        $patronageTitlesKeys[] = [$i => $patronage[0]];
                        $patronageTitlesKeys[] = [$i => $item];
                    }
                }
            }else{
                foreach ($value as $i => $item){
                    foreach ($patronageTitlesKeys as $n => $patron){
                        foreach ($patron as $e => $pat){
                            if ($i === $e){
                                if (count($reformatPatronages) === (count($value) - 1)){
                                    if (!empty($reformatPatronages[$key -1]))
                                        $reformatPatronages[$key -1][0] .= ", $pat ($item)";
                                }else{
                                    $data[] =  "$pat ($item)";
                                }
                            }
                        }
                    }
                }
                if (count($reformatPatronages) < (count($value) - 1)) {
                    $reformatPatronages[] = $data;
                }
            }
        }

        return [$reformatPatronages, $patronageTitlesKeys];
    }

    private function getUpdatedSheets($userColumnPatronagesKeys, $sheetsArray, $patronagesColumn)
    {
        foreach ($userColumnPatronagesKeys as $key => $item){
            foreach ($item as $i => $value){
                $keys[$i] = $value;
            }
        }
        foreach ($sheetsArray as $key => $value){
            foreach ($value as $i => $item){
                if (is_null($item)) {
                    unset($sheetsArray[$key][$i]);
                    continue;
                }
                if (array_key_exists($i, $keys)){
                    unset($sheetsArray[$key][$i]);
                }
            }
        }
        foreach ($sheetsArray as $i => $x)
            if ($x == []) unset($sheetsArray[$i]);

        foreach ($sheetsArray as $key => $value) {
            if ($key === 0){
                $sheetsArray[$key][] = 'Patronage';
            }else{
                if (!empty($patronagesColumn[$key -1]))
                    $sheetsArray[$key][] = $patronagesColumn[$key -1][0];
            }
        }

        return array_filter($sheetsArray, function(array $x) {return $x != [];});
    }


    /**
     * @param Request $request
     * @return array
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function getFoundColumns(Request $request)
    {
        $userColumns = $this->getUserColumns();
        $userColumnArray = [];
        foreach ($userColumns as $userColumn){
            $userColumnArray[] = $userColumn['id'];
        }

        $reader = IOFactory::load($request->file('image')->getPathname());
        $sheets = $reader->getActiveSheet();
        $sheetsArray = $sheets->toArray();
        $matchedColumnIds = [];
        $foundColumns = [];
        list($patterns, $replacements) = $this->getPatternReplacements();
        list($reformatPatronages, $patronagesKeys) =  $this->getPatronagesColumn($sheetsArray, '/\bp[ae]tronage[s]?/i');
        list($patronagesColumn, $recognitionKeys) = $this->getPatronagesColumn($sheetsArray, '/\brecogn[o,i]?[i,o]tion[s]?/i', $reformatPatronages);
        $userColumnPatronagesKeys = array_merge($patronagesKeys, $recognitionKeys);
        $updatedSheets = $this->getUpdatedSheets($userColumnPatronagesKeys, $sheetsArray, $patronagesColumn);
        foreach ($updatedSheets[0] as $key => $value){
                $val = trim($value);
                $matchedColumnIds[$key] = str_replace(' ', '_', preg_replace($patterns, $replacements, $val));
                $foundColumns[] = [
                    'name' => str_replace(' ','_', preg_replace($patterns, $replacements, $val)),
                    'similar_column_id' => preg_replace($patterns, $replacements, $val),
                    'original_name' => $val
                ];
        }


        Session::start();
        session(['matchedColumnIds' => $matchedColumnIds]);
        session(['foundColumns' => $foundColumns]);
        session(['updatedData' => $updatedSheets]);

        $samples = array_slice($updatedSheets, 1, 3);
        $analyzedData = array_map(null, ...$samples);
        $acceptancesCount = count($updatedSheets) - 1; # - headers

        return [$foundColumns, $analyzedData, $acceptancesCount];
    }


    public function handleColumnsOrder(Request $request)
    {
        $columns = $request->all();
        $updatedSheets = Session::get('updatedData');
        $foundColumns = Session::get('foundColumns');
        $matchedColumnIds = Session::get('matchedColumnIds');
        $handledRequest = $this->getHandleRequest($columns);
        $changedColumnsIds = $this->getChangedColumnIds($matchedColumnIds, $handledRequest);
        $data = [];
        $datas = [];
        $userColumnArray = [];
        foreach ($this->requiredColumns() as $userColumn){
            $userColumnArray[] = $userColumn['id'];
        }
        list($additionalColumns, $customColumnsName) = $this->additionalColumns();
        foreach ($updatedSheets as $key => $item){
            if ($key >= 1){
                foreach ($foundColumns as $i => $value){
                    if (!in_array($value['name'], $userColumnArray)){
                        if (array_key_exists($value['name'], $handledRequest) !== false
                            && ($handledRequest[$value['name']] == 'custom') || $handledRequest[$value['name']] == $value['name']
                        || in_array($value['name'], $customColumnsName)){
                            if ($handledRequest[$value['name']] !== null){
                                if (isset($item[$changedColumnsIds[$i]]))
                                    $data['acceptance-custom-' . lcfirst($value['name'])] =  trim( $item[$changedColumnsIds[$i]]);
                            }
                        }
                    }else{
                        if (isset($item[$changedColumnsIds[$i]]))
                            $data['acceptance-' . lcfirst($value['name'])] =  trim($item[$changedColumnsIds[$i]]);
                    }
                }
                $datas[] = $data;
            }
        }

        foreach ($datas as &$element) {
            if (!isset ($element['acceptances-award']))
                $element['acceptance-award'] = null;
            if (!isset ($element['acceptances-country']))
                $element['acceptance-country'] = null;
        }

//        dd($datas);

        return $datas;
    }

    private function getHandleRequest($columns)
    {
        $handledRequest = [];

        foreach ($columns as $key => $item){
            $handledRequest[$key] = $item;
            if ($item === 'custom'){
                Column::updateOrCreate([
                    'user_id' => Auth::id(),
                    'name' => $key
                ]);
            }
        }

        return $handledRequest;
    }

    private function getChangedColumnIds($matchedColumnIds, $handledRequest)
    {
        $changedColumnsIds = [];
        $request = $handledRequest;
        foreach ($matchedColumnIds as $key => $value){
            if ($handledRequest[$value] === 'custom'){
                $val = array_search('custom', $request);
                $changedColumnsIds[] = array_search($val, $matchedColumnIds);
                unset($request[$val]);
            }else{
                $changedColumnsIds[] = array_search($handledRequest[$value], $matchedColumnIds);
            }
        }
        return $changedColumnsIds;
    }
}