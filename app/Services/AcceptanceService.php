<?php

namespace App\Services;

use App\Column;
use App\ColumnsValues;
use App\Country;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AcceptanceService
{
    public function acceptanceTypes()
    {
        $types = [
            'country' => 'country',
            'entry_fee' => 'entry_fee',
            'best_author' => 'best_author'
        ];

        return $types;
    }

    public function getRelationship(): array
    {
        $relationship = [];
        $requiredNames = ['ID', 'Salon_id','Salon name', 'Patronages', 'Country', 'Image title', 'Award'];

        $additionalColumns = Column::where('user_id', Auth::user()->id)->get();

        if (!empty($additionalColumns)){
            foreach ($additionalColumns as $column){
                $requiredNames[] = $column->name;
            }
        }
        return [$relationship, $requiredNames];
    }


    public function getNames(): array
    {
        $relationship = [];
        $names = [];
        $requiredNames = ['ID', 'Salon_id', 'Salon name', 'Patronages', 'Country', 'Image title', 'Award'];
        foreach ($requiredNames as $name){
            $names[] =
                [
                    'id' => 0,
                    'name' => $name
                ];
        }
        $additionalColumns = Column::where('user_id', Auth::user()->id)->get();
        if (!empty($additionalColumns)){
            foreach ($additionalColumns as $column){
                $names[] =
                    [
                        'id' => $column->id,
                        'name' => $column->name
                    ];
            }
        }
        return [$relationship, $names];
    }

    public function formatData(Collection $datas, array $relationship): array {
        $data = [];

        foreach ($datas as $key => $item){
            foreach ($item -> acceptances as $acceptance) {
                $country = Country::where('id', $item->country_id)->first();
                $i = [
                    'id' => $acceptance->id,
                    'salon_id' => $item->id,
                    'name' => $item->name,
                    'patronages' => $item->patronages_formatted,
                    'country' => !empty($country->name) ? $country->name : null,
                    'photo-title' => $acceptance -> participation -> photo_title,
                    'award' => $acceptance->award,
                ];
                $data[] = $this->fillCustomColumn($acceptance, $i);

            }
        }
        return $data;
    }

    public function fillCustomColumn($acceptance, array $i)
    {
        $columns = Column::with('column_values')->where('user_id', Auth::user()->id)->get();
        foreach ($columns as $column){
            $i[$column->name] = '';
        }
        $columnsCheck = ColumnsValues::all(['acceptance_id'])->toArray();
        $array = Arr::dot($columnsCheck);
        if (in_array($acceptance->id, $array)){
            $columns = ColumnsValues::all()->where('acceptance_id','=', $acceptance->id);
            foreach ($columns as $relation){
                $i[$relation->column->name] = $relation->value;
            }
        }
        return $i;
    }

    private function applyAcceptanceFilters(Relation $q, Request $request) {

        $q -> whereHas('participation', function(Builder $q) use ($request) {
            $this -> applyParticipationFilters($q, $request);
        });

        if (!empty($salonAward = $request->get('filter-award'))){
            $q -> where('award', 'like', "%$salonAward%");
        }

        if (!empty($request->get('filter-is-award')) || !empty($request->get('filter-image-title-award'))){
           $q -> where('award', '!=', null)
                -> where('award', '!=', '');
        }

        if ($request->get('without-award')){
            $q -> where('award', '=', null)
            ->orWhere('award', '=', '');
        }

        if ($specificAward = $request->get('specific-award')){
            $q -> where('award', '=', $specificAward);
        }

    }

    private function applyParticipationFilters(Builder $q, Request $request) {
        if (!empty($photoTitle = $request->get('filter-image-title'))){
            $q -> where('photo_title', 'like', "%$photoTitle%");
        }

        if (!empty($photoTitle = $request->get('filter-image-title-award'))){
            $q -> where('photo_title', 'like', "%$photoTitle%");
        }
    }

    public function applySalonsFilters(Relation $builder, Request $request)
    {
//        if (!empty($request->get('search'))){
//            return $this->mainFilter($builder, $request);
//        }


        if (!empty($salonName = $request->get('filter-salon-name'))){
            $builder = $builder
                ->where('name', 'like', "%$salonName%");
        }

        if (!empty($salonCountry = $request->get('filter-country'))){
            $countries = Country::where('name', 'like', "%$salonCountry%")->get()->toArray();
            if($countries)
                $builder = $builder
                    ->whereIn('country_id', $countries);
        }


        if (!empty($participatedAfter = $request->get('filter-after-date')) &&
            !empty($participatedBefore = $request->get('filter-before-date'))){

            $builder = $builder

                ->whereBetween('closing_date',array($participatedAfter, $participatedBefore));
        }else{
            if (!empty($participatedAfter = $request->get('filter-after-date'))){
                $builder = $builder
                    ->where('closing_date', '>', $participatedAfter);
            }

            if (!empty($participatedBefore = $request->get('filter-before-date'))){
                $builder = $builder
                    ->where('closing_date', '<', $participatedBefore);
            }
        }

        $builder = $builder -> with(['acceptances' => function(Relation $q) use ($request) {
            $this -> applyAcceptanceFilters($q, $request);
        }]);

        if (!empty($organization = $request->get('organization'))) {
            $builder = $builder->whereHas('patronages', function ($q) use ($request) {
                $this->applyPatronagesFilters($q, $request);
            });
        }

        return $builder;
    }

//    public function mainFilter(Builder $builder, Request $request)
//    {
//        if (!empty($salonName = $request->get('search'))){
//            $builder = $builder
//                ->where('name', 'like', "%$salonName%");
//        }
//
//        return $builder;
//    }

    private function applyPatronagesFilters(Builder $q, Request $request)
    {
        $organization = $request->get('organization');
        $q -> where('organization', '=', $organization);

    }
}
