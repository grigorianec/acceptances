<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CountriesStatsView
 *
 * @property int $id
 * @property string $name
 * @property int $award_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereAwardCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CountriesStatsView whereName($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property int $salons_count
 * @property float|null $salons_with_acceptances_count
 * @property float|null $salons_with_awards_count
 * @property float|null $salons_without_result_count
 * @property float|null $acceptances_count
 * @property int $countries_count
 * @property float|null $award_count_FIAP
 * @property float|null $award_count_PSA
 * @property float|null $awards_count
 * @property int $photos_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView whereAcceptancesCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView whereAwardCountFIAP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView whereAwardCountPSA($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView whereAwardsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView whereCountriesCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView wherePhotosCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView whereSalonsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView whereSalonsWithAcceptancesCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView whereSalonsWithAwardsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView whereSalonsWithoutResultCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UsersStatsView whereUserId($value)
 */
class UsersStatsView extends Model
{
    protected $table = 'user_stats';
}
