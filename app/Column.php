<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Column
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Column newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Column newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Column query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Column whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Column whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Column whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property string $name
 * @property string $relationship
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Column whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Column whereRelationship($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Column whereUserId($value)
 */
class Column extends Model
{
    protected $fillable = ['user_id', 'name', 'relationship'];

    public function column_values() {
        return $this -> hasOne(ColumnsValues::class);
    }
}
