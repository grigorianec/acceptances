<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $table = 'vouchers';

    protected $dates = ['deleted_at'];

    public function users() {
        return $this->belongsToMany(User::class);
    }

    public static function discountsActive() {
        return Voucher::withCount('users')->get()-> toArray();
    }
    public static function discountsNotActive() {
        return Voucher::onlyTrashed()->withCount('users')->get()-> toArray();
    }

    public static function createDiscount($discount) {
            $voucher = new static;
            $voucher-> fill($discount);
            $voucher->save();
            return $voucher;
    }

}
