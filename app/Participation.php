<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Participation
 *
 * @property int $id
 * @property string $photo_title
 * @property int $salon_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participation wherePhotoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participation whereSalonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Participation extends Model
{
    protected $fillable = ['photo_title', 'salon_id'];

    public function user() {
        return $this -> salon() -> user;
    }

    public function salon() {
        return $this -> belongsTo(Salon::class);
    }

    public function acceptances() {
        return $this -> hasMany(Acceptance::class);
    }

    public function photo() {
        return $this -> belongsTo(Photo::class, 'photo_title', 'title');
    }

    public function photoStats() {
        // Toto case insensitive
        // @see https://stackoverflow.com/questions/35288365/laravel-eloquent-case-sensitive-relationships
        return $this -> hasOne(PhotoStatsView::class,'title', 'photo_title');
    }

}
