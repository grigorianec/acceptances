<?php


namespace App\Traits;


use Illuminate\Support\Facades\Config;

trait Plan
{


    public function getPlan($request) {
        $plans = Config::get('payments.plans');

        if($request->has('months')) {
            switch ($request->months) {
                case '1':
                    $plan = $plans[0];
                    break;
                case '3':
                    $plan = $plans[1];
                    break;
                case '12':
                    $plan = $plans[2];
                    break;
            }
            return $plan;
        }
        return false;
    }
}
