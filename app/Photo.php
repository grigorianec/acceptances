<?php

namespace App;

use App\Traits\UploadTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * App\Photo
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Photo whereUserId($value)
 */
class Photo extends Model
{
    protected $fillable = ['user_id', 'title', 'image'];

    use UploadTrait;

    public function participations() {
        return $this -> hasMany(Participation::class, 'photo_title', 'title');
    }

    public function uploadImage($image) {
        if($image == null) { return; }
        if($this->image != null){
            Storage::delete($this->image);
        }
        $name = time(). $image->getClientOriginalName();
        // Define folder path
        $folder = '/uploads/images/';
        $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
        // Upload image
        $this->uploadOne($image, $folder, 'public', $name);
        $this->image = $filePath;
        $this->save();
    }
}
