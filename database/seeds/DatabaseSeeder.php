<?php

use App\Acceptance;
use App\Attachment;
use App\Column;
use App\Country;
use App\Participation;
use App\Patronage;
use App\Photo;
use App\Salon;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Countries
        foreach (json_decode(file_get_contents(base_path('countries.json')),true) as $key => $name)
            factory(Country::class) -> create(compact('name'));

        // Default admin
        factory(User::class, 1)->create();

        $user = User::find(1);
        $user -> email = 'arsengoian@gmail.com';
        $user -> password = Hash::make('arsengoian@gmail.com');
        $user->is_admin = true;
        $user->paid_until = now() -> addYears(40);
        $user -> save();


    }
}
