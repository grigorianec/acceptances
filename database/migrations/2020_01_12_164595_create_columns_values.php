<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnsValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('columns_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('value')->nullable();
            $table->unsignedBigInteger('acceptance_id');
            $table->unsignedBigInteger('column_id');
            $table->timestamps();

            $table->foreign('acceptance_id')->references('id')->on('acceptances')->onDelete('cascade');
            $table->foreign('column_id')->references('id')->on('columns')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::disableForeignKeyConstraints();
        Schema::drop('columns_values');

    }
}
