<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # TODO only account for past salons
        DB::statement("
            CREATE OR REPLACE VIEW photo_stats
            AS
            SELECT
                s.user_id as suid, photos.user_id, photos.id as id, title, photos.image as image,
                COUNT(DISTINCT a.id) AS acceptances_count,
                COUNT(a.award) AS awards_count,
                COUNT(DISTINCT p.id) AS participation_count,
                COUNT(DISTINCT s.id) AS salon_count
            FROM
                photos
                    LEFT JOIN participations p on photo_title = title
                    LEFT JOIN salons s on p.salon_id = s.id
                    LEFT JOIN acceptances a on p.id = a.participation_id
            GROUP BY photos.user_id, photos.id, s.user_id
            HAVING suid = photos.user_id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::statement("
            DROP VIEW IF EXISTS `photo_stats`;
        ");
    }
}
