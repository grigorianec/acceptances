<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW user_stats
            AS
            SELECT
                users.id AS user_id,
                COALESCE(salons_count, 0) AS salons_count,
                COALESCE(salons_with_acceptances_count, 0) AS salons_with_acceptances_count,
                COALESCE(salons_with_awards_count, 0) AS salons_with_awards_count,
                COALESCE(salons_without_result_count, 0) AS salons_without_result_count,
                COALESCE(acceptances_count, 0) AS acceptances_count,
                COALESCE(countries_count, 0) AS countries_count,
                COALESCE(awards_count, 0) AS awards_count,
                COALESCE(photos_count, 0) AS photos_count
            FROM users
                     LEFT OUTER JOIN (
                SELECT user_proto_stats.user_id                     AS user_id,
                       salons_count,
                       salons_with_acceptances_count,
                       salons_with_awards_count,
                       salons_count - salons_with_acceptances_count AS salons_without_result_count,
                       acceptances_count,
                       countries_count,
                       awards_count,
                       COUNT(DISTINCT ph.id)                        AS photos_count
                FROM (
                         SELECT g.user_id,
                                COUNT(DISTINCT g.id)                              AS salons_count,
                                COUNT(DISTINCT g.country_id)                      AS countries_count,
                                SUM(acceptances_sum)                              AS acceptances_count,
                                SUM(award_sum)                                    AS awards_count,
                                SUM(IF(award_sum > 0, 1, 0))                      AS salons_with_awards_count,
                                SUM(IF(acceptances_sum > 0, 1, 0))                AS salons_with_acceptances_count
                         FROM (
                                  SELECT s.id,
                                         s.user_id,
                                         s.country_id,
                                         COUNT(award)                         AS award_sum,
                                         COUNT(DISTINCT a.id)                 AS acceptances_sum
                                  FROM acceptances a
                                           RIGHT OUTER JOIN participations p on a.participation_id = p.id
                                           RIGHT OUTER JOIN salons s on p.salon_id = s.id
                                  WHERE
                                     # The salon has no dates, however, no is_future column is set
                                      (s.closing_date IS NULL AND s.award_date IS NULL AND s.results_date IS NULL AND NOT is_future)
                                     OR
                                     # The salon has at least one date and all dates it has are in the past
                                      ((s.closing_date IS NOT NULL OR s.award_date IS NOT NULL OR s.results_date IS NOT NULL) AND
                                            (s.closing_date IS NULL OR s.closing_date < DATE(NOW())) AND
                                 (s.award_date IS NULL OR s.award_date < DATE(NOW())) AND
                                 (s.results_date IS NULL OR s.results_date < DATE(NOW()))
                                          )
            
                                  GROUP BY s.id
                              ) AS g
                         GROUP BY user_id
                     ) AS user_proto_stats
                         LEFT JOIN photos ph ON ph.user_id = user_proto_stats.user_id
                GROUP BY user_proto_stats.user_id
            ) AS stats_not_empty ON stats_not_empty.user_id = users.id
            ORDER BY user_id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::statement("
            DROP VIEW IF EXISTS `user_stats`;
        ");
    }
}
