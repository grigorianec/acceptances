<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW salons_stats
            AS
            SELECT
                salons_id AS id, name, user_id,
                country_id,
                COALESCE(acceptances_count, 0) AS acceptances_count,
                COALESCE(award_count, 0) AS award_count,
                COALESCE(photo_count, 0) AS photo_count,
                COALESCE(entry_fee, 0) AS entry_fee,
                COALESCE(fee_currency, 0) AS fee_currency,
                COALESCE(best_author, 0) AS best_author,
                COALESCE(comment, '') AS comment,
                COALESCE(start_date, '') AS start_date,
                COALESCE(results_date, '') AS results_date,
                COALESCE(award_date, '') AS award_date,
                COALESCE(closing_date, '') AS closing_date
            FROM (
                SELECT
                    salons_id, name,
                    user_id,
                    country_id,
                    photo_count,
                    SUM(acceptances_sum) AS acceptances_count,
                    COUNT(award) AS award_count,
                    entry_fee,
                    fee_currency,
                    best_author,
                    comment,
                    start_date, results_date, award_date, closing_date
                FROM(
                    SELECT
                        user_id, name,
                        country_id,
                        COUNT(acceptances.id) AS acceptances_sum,
                        salons.id AS salons_id,
                        award,
                        entry_fee,
                        fee_currency,
                        best_author,
                        comment,
                        start_date, results_date, award_date, closing_date
                    FROM salons
                    LEFT OUTER JOIN participations ON salons.id = participations.salon_id
                    LEFT OUTER JOIN acceptances ON participations.id = acceptances.participation_id
                    WHERE
                        # The salon has no dates, however, no is_future column is set
                    (salons.closing_date IS NULL
                        AND salons.award_date IS NULL
                        AND salons.results_date IS NULL AND NOT is_future)
                        OR
                        # The salon has at least one date and all dates it has are in the past
                    (
                    (salons.closing_date IS NOT NULL
                        OR salons.award_date IS NOT NULL
                        OR salons.results_date IS NOT NULL) AND
                        (salons.closing_date IS NULL OR salons.closing_date < DATE(NOW())) AND
                        (salons.award_date IS NULL OR salons.award_date < DATE(NOW())) AND
                        (salons.results_date IS NULL OR salons.results_date < DATE(NOW()))
                    )
                    GROUP BY salons_id, award
                ) AS stats
                LEFT JOIN (
                    SELECT
                        COUNT(DISTINCT photos.id) AS photo_count,
                        salon_id
                    FROM participations
                    LEFT JOIN photos ON photos.title = participations.photo_title
                    LEFT JOIN salons ON participations.salon_id = salons.id
                    WHERE photos.user_id = salons.user_id
                    GROUP BY salon_id
                ) AS photo_stats ON stats.salons_id = photo_stats.salon_id
                GROUP BY salons_id
            ) AS salon_stats;

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::statement("
            DROP VIEW IF EXISTS `salons_stats`;
        ");
    }
}
