<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnsTable extends Migration
{
    const ALLOWED_RELATIONSHIPS = ['country', 'entry_fee', 'best_author'];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('columns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('name');
            $table->enum('relationship', self::ALLOWED_RELATIONSHIPS); // TODO убрать это
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //  DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('columns');

    }
}
