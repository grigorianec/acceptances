<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->date('patronage_FIAP')->nullable();
            $table->date('patronage_PSA')->nullable();
            $table->string('tracking_number')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->text('comment')->nullable();
            $table->integer('entry_fee')->nullable();
            $table->string('fee_currency')->nullable();
            $table->tinyInteger('best_author')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('closing_date')->nullable();
            $table->dateTime('results_date')->nullable();
            $table->dateTime('award_date')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->string('image')->nullable();
            $table->tinyInteger('viewed')->default(0);
            $table->tinyInteger('is_future')->default(0);
            $table->timestamps();


            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salons');
    }
}
