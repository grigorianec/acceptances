<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW countries_stats
            AS
            SELECT
                users.id AS user_id,
                countries.name,
                COUNT(award) AS award_count,
                COUNT(DISTINCT salons.id) AS salons_count
            FROM
                salons
                    LEFT JOIN participations p ON salons.id = p.salon_id
                    LEFT JOIN acceptances a on p.id = a.participation_id
                    LEFT JOIN users ON users.id = salons.user_id
                    LEFT JOIN countries ON countries.id = salons.country_id
            WHERE
                # The salon has no dates, however, no is_future column is set
                (salons.closing_date IS NULL AND salons.award_date IS NULL AND salons.results_date IS NULL AND NOT is_future)
                OR
                # The salon has at least one date and all dates it has are in the past
                (
					(salons.closing_date IS NOT NULL OR salons.award_date IS NOT NULL OR salons.results_date IS NOT NULL) AND
                    (salons.closing_date IS NULL OR salons.closing_date < DATE(NOW())) AND
                    (salons.award_date IS NULL OR salons.award_date < DATE(NOW())) AND
                    (salons.results_date IS NULL OR salons.results_date < DATE(NOW()))
                )
            GROUP BY
                countries.id, users.id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS `countries_stats`;
        ");
    }
}
