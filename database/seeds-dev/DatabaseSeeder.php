<?php

use App\Acceptance;
use App\Attachment;
use App\Column;
use App\Country;
use App\Participation;
use App\Patronage;
use App\Photo;
use App\Salon;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Country for acceptances
        factory(Country::class) -> create();

        // Countries
        factory(Country::class, 4)->create()->each(function (Country $country) {

            // Users
            factory(User::class, 2)->create([
                'country_id' => $country -> id
            ])->each(function (User $user) {

                echo "Generating data for $user->name $user->surname";

                //Columns
//                factory(Column::class)->create([
//                    'user_id' => $user -> id
//                ]);

                // Salon
                factory(Salon::class, 10) -> create([
                    'user_id' => $user -> id
                ])->each(function(Salon $salon) use ($user) {

                    // Patronages
                    factory(Patronage::class, 1) -> create([
                        'salon_id' => $salon -> id,
                        'organization' => 'FIAP'
                    ]);
                    factory(Patronage::class, 1) -> create([
                        'salon_id' => $salon -> id,
                        'organization' => 'PSA'
                    ]);

                    // Participation
                    if ($salon->closing_date < now() && $salon->award_date < now() && $salon->results_date < now()) {
                        factory(Participation::class, 2)->create([
                            'salon_id' => $salon -> id
                        ])->each(function(Participation $participation) use ($user) {

                            // Acceptance
                            factory(Acceptance::class)->create([
                                'participation_id' => $participation -> id
                            ]);

                            // Photo
                            factory(Photo::class)->create([
                                'title' => $participation -> photo_title,
                                'user_id' => $user -> id
                            ]);
                        });
                    }


                    //Attachment
                    factory(Attachment::class, 4)->create([
                        'salon_id' => $salon -> id
                    ]);
                    echo '.';
                });

                echo "\n";
            });
        });

        $user = User::find(7);
        $user -> email = 'arsengoian@gmail.com';
        $user -> password = Hash::make('arsengoian@gmail.com');
        $user->is_admin = true;
        $user -> save();


    }
}
