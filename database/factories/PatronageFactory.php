<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Patronage;
use Faker\Generator as Faker;

$factory->define(Patronage::class, function (Faker $faker) {
    return [
        'number' => $faker -> year.'/'.$faker->randomNumber(2)
    ];
});
