<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Country;
use App\Salon;
use Faker\Generator as Faker;

$factory->define(Salon::class, function (Faker $faker) {
    return [
        'name' => 'Global '.$faker->company.' Awards',
        'country_id' => Country::all('id') -> random() -> id,
        'comment' => $faker->realText('200'),
        'entry_fee' => $faker->randomNumber(2)*10,
        'fee_currency' => $faker->currencyCode,
        'best_author' => $faker->boolean,
        'start_date' => now()->subYears(($digit = $faker->randomDigit) - 1),
        'closing_date' => now()->addMonths(2)->subMonths(($digit = $faker->randomDigit) - 1),
        'results_date' => now()->addMonths(2)->subMonths(($digit = $faker->randomDigit) - 1),
        'award_date' => now()->addMonths(2)->subMonths(($digit = $faker->randomDigit) - 1),
        'tracking_number' => $faker->randomNumber(5).$faker->randomNumber(5),
        'image' => $faker->imageUrl(1280, 960, 'nature'),
        'catalogue_link' => $faker->url,
    ];
});
