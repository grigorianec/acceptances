<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Column;
use Faker\Generator as Faker;

$factory->define(Column::class, function (Faker $faker) {
    return [
        'relationship' => $r = ($x = ['country', 'entry_fee', 'best_author'])[rand(0, count($x) - 1)],
        'name' => $faker -> firstNameMale
    ];
});
