<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Attachment;
use Faker\Generator as Faker;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

$factory->define(Attachment::class, function (Faker $faker) {
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', $faker -> realText(300));

    $path = "/attachments/file/";
    $filename = 'Faker.xlsx';
    $dir = public_path($path);

    if (!File::isDirectory($dir)) {
        Storage::disk('public')->makeDirectory($path, 0755, true, true);
    }


    $writer = new Xlsx($spreadsheet);
    $writer->save($file = public_path($path.$filename));
   // $blob = file_get_contents($file);
  //  unlink($file);

    return [
        'type' => $t = ($x = ['ACCEPTANCE_LIST','AWARDS_LIST','REPORT_CARD'])[rand(0, count($x) - 1)],
        'title' => 'Random Attachment '.ucfirst(str_replace('_', ' ',strtolower($t))),
      //  'blob' => $blob,
        'file' => $path.$filename,
    ];
});
