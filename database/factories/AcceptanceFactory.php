<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Acceptance;
use App\Country;
use Faker\Generator as Faker;

$factory->define(Acceptance::class, function (Faker $faker) {
    return [
        'award' => ['PSA', 'FIAP Ribbon', NULL][rand(0,2)],
    ];
});
