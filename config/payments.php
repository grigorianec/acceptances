<?php

return [

    'plans' => [
        [
            'months' => 1,
            'price'  => 1,
            'description' => '$1 monthly'
        ],
        [
            'months' => 3,
            'price'  => 15,
            'description' => '$15 quarterly'
        ],
        [
            'months' => 12,
            'price'  => 60,
            'description' => '$60 yearly'
        ],
    ],

];

