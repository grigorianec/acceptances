addEventListener('DOMContentLoaded', function () {
    // if user clicks on any 'delete discount' link,
    // we add discount id to modal
    addDeleteLinksIdToModal();
});

function addDeleteLinksIdToModal() {
    const deleteLinks = document.querySelectorAll('a[href="#delete-discount-modal"]');
    const modal = document.getElementById('delete-discount-modal');
    const inputId = modal.querySelector('#delete_discount_id');

    if (!deleteLinks.length)
        return;

    for (let link of deleteLinks) {
        link.addEventListener('click', function() {
            inputId.value = this.dataset.id;
        })
    }
}
