// ACCEPTANCE MODAL ELEMENTS
const $acceptanceModal = $('#acceptance-modal');
const $addPatronagesModal = $('#add-patronages-modal');
const acceptanceForm = document.getElementById('acceptance-form');
const addPatronagesForm = document.getElementById('add-patronages-form');
const inputs = document.querySelectorAll('#acceptance-modal input, #acceptance-modal select');
const $salonNameInput = $('#acceptance-salon-name');
const idInput = document.getElementById('acceptance-id');
const salonIdInput = document.getElementById('acceptance-salon-id');
const patronagesInput = document.getElementById('acceptance-patronages');
const $countryInput = $('#acceptance-country');
const acceptanceModalHeading = $acceptanceModal[0].querySelector('.modal__heading');
const acceptanceModalSubmit = document.getElementById('acceptance-submit');

// COLUMNS MODAL ELEMENTS
const editColumnButtons = document.querySelectorAll('a[href="#edit-column-modal"][rel="modal:open"]');
const columnNameInput = document.getElementById('edit-column-name');

// TABLE ELEMENTS
const table = initiateFooTable(); // it initiates almost all important functions in the acceptance table

// JS ELEMENTS
const addAcceptanceButton = document.querySelector('.js-add-acceptance');

// CONDITIONS
let isUserDeletedAcceptance, isResetPrevented;

// AJAX ARRAYS
let pastSalons;

// ASYNC AJAX CALLS
getPastSalons(); // get all the salons the user have from API

addEventListener('DOMContentLoaded', function () {
    // open acceptance modal on 'add acceptance' top button
    // bottom button is controlled by table initiation (editing: {..., addRow:...})
    addAcceptanceButton.addEventListener('click', function() {
        openAcceptanceModal('Add');
    });

    // reset acceptance every time we close the modal
    // we don't reset acceptance only when user open and closes patronages form
    $acceptanceModal.on($.modal.CLOSE, function() {
        if (isResetPrevented)
            return;

        resetAcceptanceModal();
    });

    // we monitor the salon name select in acceptance modal
    // if it changes and a user chooses existing salon
    // we disable country and patronages inputs
    $salonNameInput.on('change', fixExistingSalon);

    // when user focuses on patronages input
    // we open the patronages modal
    patronagesInput.addEventListener('focus', openPatronagesModal);

    // when user submits patronages form
    // we close the modal
    // and format the patronages for him and for database
    addPatronagesForm.addEventListener('submit', function(e) {
        if (this.checkValidity && !this.checkValidity()) return; // if validation fails exit early and do nothing.
        e.preventDefault(); // stop the default post back from a form submit

        // we add patronages to acceptance form
        addPatronagesToAcceptanceForm(this);

        closePatronagesModal();
    });

    // when user submits acceptance form
    // we add or edit the acceptance using ajax
    acceptanceForm.addEventListener('submit', function(e) {
        if (this.checkValidity && !this.checkValidity()) return; // if validation fails exit early and do nothing.
        e.preventDefault(); // stop the default post back from a form submit

        submitForm();
    });

    // when user clicks 'edit' on column
    // we add the column name to a modal's input
    for (let editColumnButton of editColumnButtons) {
        editColumnButton.addEventListener('click', function() {
            columnNameInput.value = editColumnButton.dataset.name;
        });
    }
});

/**
 * Submits data to client and to server.
 * Based on data-row in $acceptanceModal element,
 * it decides whether it's editing or adding operation
 */
function submitForm() {
    let $row = $acceptanceModal.data('row'), // get any previously stored row object
        values = {},
        ids = {},
        patronages,
        method = "add", // a method by default
        url = "/app/acceptances/add-accept"; // an ajax url by default

    if ($row instanceof FooTable.Row) { // if we have a row object then this is an edit operation
        url = "/app/acceptances/update-accept";
        method = 'edit';
    }

    // we put every value we have in the inputs to an array
    // in order to use the array in editing or adding operation
    inputs.forEach(function(item, i, array) {
        if (item.type === 'submit')
            return;

        if (item.name === 'acceptance-country' || item.name === 'acceptance-salon-name') {
            // we get name for the row
            values[item.name] = item.options[item.options.selectedIndex].text;

            // we get ids for database
            ids[item.name] = item.value;
            return;
        }

        if (item.name === 'acceptance-patronages') {
            patronages = $(item).data('array');
        }

        values[item.name] = item.value;
    });

    let countryName = values['acceptance-country'],
        patronagesFormatted = values['acceptance-patronages'];

    // add country id to the array
    values['acceptance-country'] = ids['acceptance-country'];

    // add patronages to the array
    // values['acceptance-patronages'] = patronages;

    // send values via ajax
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: url,
        data: {method: method, values: values}
    }).done(function(data) {
        let isNewSalon = parseInt(values['acceptance-salon-id']) !== parseInt(data['salon_id']); // check if that's a new salon

        if (isNewSalon) { // if user creates new salon
            // assign new salon id to the row
            values['acceptance-salon-id'] = data['salon_id'];

            // add the salon to the past salons list
            pastSalons.push({
                id: values['acceptance-salon-id'],
                'salon_name': values['acceptance-salon-name'],
                'patronages': patronagesFormatted,
                'country_id': ids['acceptance-country']
            });

            // dynamically create an option for select
            let newOption = new Option(values['acceptance-salon-name'], values['acceptance-salon-id'], false, false);
            $salonNameInput.append(newOption).trigger('change');
        }

        // add formatted patronages to the row
        values['acceptance-patronages'] = patronagesFormatted;

        // add country name to the row
        values['acceptance-country'] = countryName;

        if (method === "add") { // if user adds the acceptance
            // assign new id to the row
            values['acceptance-id'] = data['acceptance_id'];

            // add actions
            values['actions'] = '<a href="#" data-id="' + data['acceptance_id'] + '" class="acceptances-table__edit js-edit-acceptance">Edit</a>';
            values['actions'] += '<a href="#" data-id="' + data['acceptance_id'] + '" class="acceptances-table__delete js-delete-acceptance">Delete</a>';

            // add this array to the table
            table.rows.add(values);
        } else { // if user edits the acceptance
            $row.val(values); // we change values of the row
        }

        refreshAcceptancesTotal();
    });

    $.modal.close();
}

/**
 * Resets the acceptance modal,
 * including acceptance form and patronages form.
 */
function resetAcceptanceModal() {
    // reset patronages form
    addPatronagesForm.reset();
    removePatronageItemsExceptFirst();

    // reset hidden inputs
    idInput.value = '';
    salonIdInput.value = '';

    // reset acceptance form
    acceptanceForm.reset();
    $acceptanceModal.data('row', '');
}

/**
 * Initiates the FooTable plugin.
 * It adds pagination, editing, sorting to the table.
 */
function initiateFooTable() {
    return FooTable.init('.acceptances-table table', {
        sorting: true,
        paging: {
            enabled: true,
            size: 20,
            container: ".acceptances-table__pagination"
        },
        editing: {
            enabled: true,
            alwaysShow: true,
            addRow: function() {openAcceptanceModal('Add')},
            addText: "Add an acceptance"
        },
        rows: {
            emptyString: 'Add your acceptances here'
        },
        on: {
            'ready.ft.table': function (e, ft) {
                refreshButtons();
            },
            'postdraw.ft.table': function (e, ft) {
                // and we refresh buttons every time table's changing
                setTimeout(function() {
                    refreshButtons();
                }, 500);
            }
        }
    });
}

/**
 * When user chooses salon in Salon name select
 * it disables country and patronages inputs
 * and fills them with values.
 */
function fixExistingSalon() {
    // find salon with needed id
    let salonId = $salonNameInput[0].value,
        checkedSalon;

    for (let salon of pastSalons) {
        if (parseInt(salon['id']) === parseInt(salonId)) {
            checkedSalon = salon;
            break;
        }
    }

    if (checkedSalon) {
        // set salon id
        salonIdInput.value = checkedSalon['id'];

        // set patronages and disable the patronages input
        patronagesInput.value = checkedSalon['patronages'];
        patronagesInput.disabled = true;

        // set country and disable the country input
        $countryInput.val(checkedSalon['country_id']);
        $countryInput.trigger('change');
        $countryInput.prop("disabled", true);
    } else {
        // if we have empty select or new item, enable the inputs
        // and clear them
        salonIdInput.value = '';

        patronagesInput.value = '';
        patronagesInput.disabled = false;

        $countryInput.val('');
        $countryInput.trigger('change');
        $countryInput.prop("disabled", false);
    }
}

/**
 * Formats patronages and adds them to acceptance form.
 */
function addPatronagesToAcceptanceForm(patronagesForm) {
    const organizationInputs = patronagesForm.querySelectorAll('input[name="organizations[]"]');
    const numberInputs = patronagesForm.querySelectorAll('input[name="numbers[]"]');
    const patronagesInput = acceptanceForm.querySelector('#acceptance-patronages');

    // create a patronages array in {organization: ..., number: ...} format
    let patronages = [];
    organizationInputs.forEach(function(organizationInput, i, array) {
        patronages.push({organization: organizationInput.value, number: numberInputs[i].value});
    });

    // format the array to put it into the acceptance form
    let patronagesFormatted = [];
    patronages.forEach(function(patronage, i, array) {
        patronagesFormatted.push(patronage['organization'] + ' (' + patronage['number'] + ')');
    });

    patronagesInput.value = patronagesFormatted.join(', ');
    $(patronagesInput).data('array', patronages);
}

/**
 * Adds click event listener to 'edit acceptance' and 'delete acceptance' buttons.
 */
function refreshButtons() {
    let acceptanceButtons = document.querySelectorAll('.js-edit-acceptance, .js-delete-acceptance');

    // open acceptance modal on 'edit acceptance' or 'delete acceptance' button
    for (let button of acceptanceButtons) {
        button.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            if (this.classList.contains('js-edit-acceptance')) {
                editAcceptance(this.dataset.id);
            } else {
                deleteAcceptance(this.dataset.id);
            }
        })
    }
}

/**
 * Changes number in 'Total acceptances' section of table.
 */
function refreshAcceptancesTotal() {
    const acceptancesTotalItem = document.querySelector('.js-acceptances-total-number');

    acceptancesTotalItem.innerHTML = getAcceptancesTotalNum();
}

/**
 * Get number of rows (acceptances) in the table.
 *
 * @returns {number} Number of acceptances in the table.
 */
function getAcceptancesTotalNum() {
    return table.rows.all.length;
}

/**
 * Opens an acceptance modal and changes it
 * based on 'operation' variable.
 *
 * @param {string} operation 'Add' or 'Edit'.
 */
function openAcceptanceModal(operation) {
    // change them modal's heading and submit text
    // if different operations
    if (operation === 'Add') {
        acceptanceModalHeading.textContent = 'Add an acceptance';
        acceptanceModalSubmit.value = 'Add';
    } else {
        acceptanceModalHeading.textContent = 'Edit the acceptance';
        acceptanceModalSubmit.value = 'Edit';
    }

    // open the modal
    $acceptanceModal.modal();
}

/**
 * Opens patronages modal.
 */
function openPatronagesModal() {
    // we prevent acceptance modal from resetting
    isResetPrevented = true;

    $addPatronagesModal.modal();
}

/**
 * Closes patronages modal.
 */
function closePatronagesModal() {
    // we allow patronages form to reset on closing
    isResetPrevented = false;

    let $row = $acceptanceModal.data('row'); // get any previously stored row object in acceptance modal

    // close the patronages modal
    // and open the acceptance modal

    // check if we add or edit the acceptance
    if ($row instanceof FooTable.Row) // if we have a row object then this is an edit operation
        openAcceptanceModal('Edit');
    else
        openAcceptanceModal('Add');
}

/**
 * Opens acceptance modal and inserts every value from the row to the form.
 *
 * @param {number} id Acceptance's id
 */
function editAcceptance(id) {
    let row = findRowById(id);

    openAcceptanceModal('Edit');

    // put every value from the row to the modal form
    putValuesToForm(row);

    // we add the row item to acceptance modal's data
    // in order to access it from other function
    $acceptanceModal.data('row', row);
}

/**
 * Finds row in the table with needed id.
 *
 * @param {number} id Acceptance's id
 * @returns {object} Row Object
 */
function findRowById(id) {
    // find a row with needed id
    for (let i = 0; i < table.rows.array.length; i++) {
        if (parseInt(table.rows.array[i].value['acceptance-id']) === parseInt(id)) {
            return table.rows.array[i];
        }
    }
}

/**
 * Puts values from a row to the acceptance form.
 *
 * @param {object} row Row Object
 */
function putValuesToForm(row) {
    inputs.forEach(function(item, i, array) {
        // if it's submit input, we don't put any value
        // the same goes for Country or Patronages, because it will be filled when we set salon name
        if (item.type === 'submit' || item.name === 'acceptance-country' || item.name === 'acceptance-patronages')
            return;

        if (item.name === 'acceptance-salon-name' ) {
            putValueToSelect2(item.name, row.value['acceptance-salon-id']);
            return;
        }

        item.value = row.value[item.name];
    });
}

/**
 * Puts values from a row to the acceptance form.
 *
 * @param {number} id Acceptance's id
 */
function deleteAcceptance(id) {
    if (!isUserDeletedAcceptance) {
        isUserDeletedAcceptance = confirm('Are you sure you want to delete the acceptance?');

        // if he's not sure, do nothing
        if (!isUserDeletedAcceptance)
            return;
    }

    let row = findRowById(id);

    // send values via ajax
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "DELETE",
        url: "/app/acceptances/" + id + '/destroy'
    }).done(function() {
        row.delete();

        refreshAcceptancesTotal();
    });
}

/**
 * Puts past salons values to 'pastSalons' variable.
 */
function getPastSalons() {
    $.get('/api/past-salons', function(data) {
        pastSalons = data;
    });
}