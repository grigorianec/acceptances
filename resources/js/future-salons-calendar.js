document.addEventListener('DOMContentLoaded', function() {
    let calendarEl = document.getElementById('calendar');

    let calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'dayGrid' ],
        header: {
            center: 'dayGridMonth,dayGridWeek,dayGridThreeDays' // buttons for switching between views
        },
        defaultView: 'dayGridMonth',
        views: {
            dayGridThreeDays: {
                type: 'dayGrid',
                duration: { days: 3 },
                buttonText: '3 days'
            }
        }
    });

    let windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

    // if window is less than 600px, we change the view to mobile adapted version
    if (windowWidth < 600) {
        calendar.changeView('dayGridThreeDays');
    }

    // get the future salons from API
    $.ajax({
        type: "GET",
        url: "/api/future-salons"
    }).done(function( data ) {
        let salons = data['salons'];

        salons.forEach(function(item){
            let dates = [];

            if (item['closing_date'])
                dates.push(moment(item['closing_date']).toDate());

            if (item['results_date'])
                dates.push(moment(item['results_date']).toDate());

            if (item['award_date'])
                dates.push(moment(item['award_date']).toDate());

            // if the array is empty, we do nothing
            if (!dates.length)
                return;

            // let's find the nearest and the farthest date
            let startDate = findStartDate(dates),
                endDate = findEndDate(dates);

            // add an event
            calendar.addEvent({
                'id': item['id'],
                'title': item['name'],
                'start': dateToYMD(startDate),
                'end': dateToYMD(endDate),
                'url': '/app/future-salons/future-salon/' + item['id']
            })
        })
    });

    calendar.render();
});