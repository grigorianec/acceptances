@component('mail::message')
    <p>
        Notice: salon {{$salon}} closes on {{$closeDate->format('j-F-Y')}}
    </p>
@endcomponent
