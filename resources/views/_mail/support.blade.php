@component('mail::message')
    Hello <i>{{ $name }}</i>,
    <p>This is a demo email for testing purposes! Also, it's the HTML version.</p>
    Demo object values:
    Demo Body:{{ $body}}
    test Id user:{{ $id }}
    test email user :{{ $email }}
    Thank You,
@endcomponent
