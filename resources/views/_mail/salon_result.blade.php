@component('mail::message')
    <p>
        Notice: the result of the salon {{$salon}} will be on  {{$result_date->format('j-F-Y')}}
    </p>
@endcomponent
