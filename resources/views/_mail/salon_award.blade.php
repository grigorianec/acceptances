@component('mail::message')
    <p>
        Notice: the award of the salon {{$salon}} will be on  {{$award_date->format('j-F-Y')}}
    </p>
@endcomponent
