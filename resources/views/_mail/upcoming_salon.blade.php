@component('mail::message')
    <p>
        Notice: salon {{$salon}} start {{$start_date->format('j-F-Y')}}
    </p>
@endcomponent
