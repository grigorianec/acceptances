@component('mail::message')
    <p>
        Your payment would be expiring {{$expired->format('j-F-Y')}} ,  and we wanted you to know!
    </p>
@endcomponent
