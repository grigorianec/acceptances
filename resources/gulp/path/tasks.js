module.exports = [
    './resources/gulp/tasks/clean',
    './resources/gulp/tasks/fonts',
    './resources/gulp/tasks/image',
    './resources/gulp/tasks/pug',
    './resources/gulp/tasks/scripts',
    './resources/gulp/tasks/serve',
    './resources/gulp/tasks/styles',
    './resources/gulp/tasks/svg',
    './resources/gulp/tasks/watch'
];