var svgmin = require('gulp-svgmin'),
    cheerio = require('gulp-cheerio'),
    replace = require('gulp-replace'),
    svgPath = {
        "input": "./resources/images/svg/*.svg",
        "output": "./public/static/images/svg/"
    };

module.exports = function () {
    $.gulp.task('svg', function () {
        return $.gulp.src(svgPath.input)
            .pipe(svgmin({
                js2svg: {
                    pretty: true
                }
            }))
            .pipe($.gulp.dest(svgPath.output));
    });
};
