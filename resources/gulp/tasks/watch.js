module.exports = function () {
    $.gulp.task('watch', function () {
        // $.gulp.watch('./dev/pug/**/*.pug', $.gulp.series('pug'));
        $.gulp.watch('./resources/styles/**/*.scss', $.gulp.series('styles:dev'));
        $.gulp.watch(['./resources/images/general/**/*.{png,jpg,gif}',
            './resources/images/content/**/*.{png,jpg,gif}'], $.gulp.series('img:dev'));
        $.gulp.watch('./resources/images/svg/*.svg', $.gulp.series('svg'));
        $.gulp.watch('./resources/js/**/*.js', $.gulp.series('js:dev'));
    });
};