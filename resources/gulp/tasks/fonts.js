module.exports = function () {
    $.gulp.task('fonts', () => {
        return $.gulp.src('./resources/fonts/**/*.*')
            .pipe($.gulp.dest('./public/static/fonts/'));
    });
};